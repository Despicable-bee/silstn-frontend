# PART 1 - COMPILE THE APPLICATION
# Pulls the node image at version 14 from dockerhub, then names it 
#   'buildContainer'.
FROM node:14 as buildContainer

# Sets the working directory of all the following commands to the '/app'
#   directory of the image
WORKDIR /app

# Copy 'package.json' and 'package-lock.json' to the 'app' directory of our
#   build container.
COPY ./package.json /app/

# Run npm install to install all the packages needed for the app to compile.
RUN npm install

# Copy all the angular source code into the 'buildContainer' image (note), 
#   .dockerignore prevents the programmers local 'node_modules' file from being
#   copied over.
COPY . /app

# Sets the memory that can be used to 2GB, to avoid the build process running out
#   of memory (This may happen with larger angular projects, aka ours)
# max-old-space is needed to avoid any compilation issues because of missing memory
ENV NODE_OPTIONS --max-old-space-size=2048

# Compiles the angular app and should create the 'dist' directory in our
#   'buildContainer'.
RUN npm run build:ssr


# PART 2 - CLEANING UP OUR DISTRIBUTION FILE
# alpine version of node.js (leaner node.js distribution, faster load times)
#   Based off the popular alpine linux project (~5MB)
FROM node:14-alpine

# Setting the new containers working directory (just like before)
WORKDIR /app

# Copy the package.json from the 'buildContainer', so we can later use npm
#   commands to serve the angular app.
COPY --from=buildContainer /app/package.json /app

# Copy the 'dist' directory fro the 'buildContainer', which contains the
#   compiled angular universal app
COPY --from=buildContainer /app/dist /app/dist

# The 'serve:ssr' hosts the universal app on port 4000, so we tell Docker to
#   expose port 4000 on our Docker image.
EXPOSE 4000

# Since Angular Universal uses Express for serving, we enable 'production mode'
#   for node.js to get the benefits
#
# Setting NODE_ENV to 'production' makes express:
#   Cache view templates.
#   Cache CSS files generated from CSS extensions
#   Generate less verbose error messages
ENV NODE_ENV production

# Tell our docker image to run 'npm run serve:ssr', which starts the angular
#   universal server on startup of the image.
CMD ["npm", "run", "serve:ssr"]