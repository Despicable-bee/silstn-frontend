export enum PageId {
    HOME,
    LOGIN,
    PROFILE,
    UNKNOWN
}
