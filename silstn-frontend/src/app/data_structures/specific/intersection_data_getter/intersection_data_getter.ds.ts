

// ENUMS -----------------------------------------------------------------------

export enum IntersectionTimeRange {
    LESS_THAN_ONE_HOUR = 1,
    ONE_HOUR,
    SIX_HOURS,
    TWELVE_HOURS,
    ONE_DAY,
    ONE_WEEK,
    ONE_MONTH,
    ONE_YEAR,
    ALL_TIME
}

export enum CheckAvailableRangesStatus {
    SUCCESSFUL = 1,
    FAILURE,
    ERROR
}

// DATA STRUCTURES -------------------------------------------------------------

export class IntersectionDataGetterRequestContainer {
    timeRange: IntersectionTimeRange;
    tsc: number;
}

export class IntersectionData {
    name: string;
    series: IntersectionDataPair[];
}

export class IntersectionDataPair {
    name: string;   // Unix timestamp in datetime format
    value: number;  // The cmf value
}

export class IntersectionColourContainer {
    // The name of the series we want to colour
    name: string;
    // Colour hex value
    value: string;
  }
  
export class GetForecastRequestContainer {
    tsc: number;
}