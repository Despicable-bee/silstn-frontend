import {
    ChunkType
} from 'src/app/data_structures/shared/agm-map/agm_map.ds';

// ENUMS -----------------------------------------------------------------------

export enum GrabChunksStatus {
    SUCCESSFUL = 1,
    FAILURE,
    ERROR
}

// DATA STRUCTURES -------------------------------------------------------------

export class Viewport {
    maxLat: number;
    maxLng: number;
    minLat: number;
    minLng: number;
    chunkType: ChunkType;
}