// * ENUMS ---------------------------------------------------------------------
export enum UserLoginRequestType {
    LOGIN_W_FORM = 0,
    LOGIN_W_JWT = 1
}

export enum UserLoginStatus {
    SUCCESSFUL = 1,
    FAILURE = 2,
    ERROR = 3 
}

export enum UserCreateAccountStatus {
    SUCCESSFUL = 1,
    INVALID_INPUT = 2,
    EMAIL_OR_MOBILE_EXISTS = 3,
    ERROR = 4
}

// * DATA STRUCTS --------------------------------------------------------------

export class UserLoginRequestDatastructure {
    jwtOld: string | undefined;
    refreshOld: string | undefined;
    email: string | undefined;
    password: string | undefined;
    loginType: UserLoginRequestType
}

export class UserCreateAccountRequestDataStructure {
    firstname: string;
    surname: string;
    email: string;
    dob: string;
    password: string;
}

// This is more so what you can expect the response object to look like for
// create account request.
export class UserCreateAccountResponseDataStructure {
    status: UserCreateAccountStatus;        // 0
    jwt: string;                            // 1
    refresh: string;                        // 2
    errorMsg: string;                       // 3
}
