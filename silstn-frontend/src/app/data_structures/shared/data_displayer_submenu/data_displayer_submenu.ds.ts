// ENUMS -----------------------------------------------------------------------

export enum SubMenuTypes {
    EMA = 1,
    SMA,
    BOLLINGER
}

export enum BollingerMAType {
    EMA = 1,
    SMA
}

// DATA STRUCTURES -------------------------------------------------------------

export class SubMenu_EMA_StateData {
    smoothingFactor: number;
    periodNumber: number;
}

export class SubMenu_SMA_StateData {
    periodNumber: number;
}

export class SubMenu_BOLLINGER_StateData {
    // Moving average type
    movingAverageType: BollingerMAType;
    // Number of periods in the moving average
    periodNumber: number;
    // Smoothing factor (only used if we're in EMA mode)
    smoothingFactor: number;
    // Number of standard deviations
    stdevNumber: number;
}

export class SubMenuState {
    name: string;
    submenuType: SubMenuTypes;
    data: SubMenu_EMA_StateData | SubMenu_SMA_StateData | SubMenu_BOLLINGER_StateData;
}
