

// * ENUMS ---------------------------------------------------------------------

export enum RoadDetail {
    // 0 - 10 km
    CLOSE,
    // 11 - 50 km
    NEAR,
    // 51 - 100 km
    STANDARD,
    // 100+ km
    FAR
}

export enum ChunkType {
    FAR = 1,
    STANDARD,
    NEAR,
    CLOSE
}

// * DATA STRUCTURES -----------------------------------------------------------

export class Chunk {
    type: ChunkType;
    latitude: number;
    longitude: number;
    topRightBounds: LatLng;
    bottomLeftBounds: LatLng;
}

export class LatLng {
    latitude: number;
    longitude: number;
    show: boolean | undefined;
}

export class LatLngLiteral {
    lat: number;
    lng: number;
}

export class Step {
    coords: LatLngLiteral[];
    distanceValue: number;
    durationValue: number;
}

export class Route {
    steps: Step[];
}

export class Arms {
    route: Route;
    nextTsc: number;
    streetName: string;
    colour: string;
    originTsc: number | undefined;
    forecastAvailable: boolean;
}

export class Intersection {
    arms: Arms[];
    origin: LatLngLiteral;
    tsc: number;
    colour: string;
    forecastAvailable: boolean;
}