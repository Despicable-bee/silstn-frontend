import {
    ThemePalette
  } from '@angular/material/core';
  
  import {
    ProgressSpinnerMode
  } from '@angular/material/progress-spinner';

export class ProgressSpinnerConfig {
    colour: ThemePalette;
    mode: ProgressSpinnerMode;
    value: number;
    diameter: number;
}
