// ANGULAR ---------------------------------------------------------------------

import { 
  Component, 
  OnInit,
  ViewChild,
  Output, 
  EventEmitter
} from '@angular/core';

import { multi } from './test_data';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
  GeneralMethodsService
} from 'src/app/services/helper_methods/shared/general_methods/general-methods.service';

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

// INTERFACES ------------------------------------------------------------------

import {
  CheckAvailableRanges,
  GetIntersectionData,
  GetForcast
} from 'src/app/services/api_calls/specific/intersection_data_getter/intersection_data_getter.interface';

// APIS ------------------------------------------------------------------------

import {
  IntersectionDataGetterService
} from 'src/app/services/api_calls/specific/intersection_data_getter/intersection-data-getter.service';

import {
  LoginApiService
} from 'src/app/services/api_calls/specific/login_api/login-api.service';

// COMPONENTS ------------------------------------------------------------------

import {
  LoadingSpinnerComponent
} from 'src/app/subcomponents/loading-spinner/loading-spinner.component';

import { 
  CustomButtonComponent 
} from 'src/app/subcomponents/custom-button/custom-button.component';

// ENUMS -----------------------------------------------------------------------

import {
    GetForecastRequestContainer,
  IntersectionDataGetterRequestContainer,
  IntersectionTimeRange
} from 'src/app/data_structures/specific/intersection_data_getter/intersection_data_getter.ds';

// DATA STRUCTURES -------------------------------------------------------------

import { 
  CustomButtonConfig, 
  CustomButtonType 
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';

import { 
  Arms 
} from 'src/app/data_structures/shared/agm-map/agm_map.ds';

import { ApiIdMaster } from 'src/app/services/api_id_master.ds';

import {
  IntersectionData,
  IntersectionDataPair,
  IntersectionColourContainer
} from 'src/app/data_structures/specific/intersection_data_getter/intersection_data_getter.ds';

import {
  ProgressSpinnerConfig
} from 'src/app/data_structures/shared/loading-spinner/loading-spinner.ds';

import { 
    BollingerMAType,
    SubMenuState, 
    SubMenuTypes, 
    SubMenu_BOLLINGER_StateData, 
    SubMenu_EMA_StateData, 
    SubMenu_SMA_StateData 
} from 'src/app/data_structures/shared/data_displayer_submenu/data_displayer_submenu.ds';


@Component({
  selector: 'app-data-displayer',
  templateUrl: './data-displayer.component.html',
  styleUrls: ['./data-displayer.component.scss']
})
export class DataDisplayerComponent implements OnInit, CheckAvailableRanges,
    GetIntersectionData, GetForcast {
  
  @ViewChild('chartLoadingSpinner') loadingSpinnerRef: LoadingSpinnerComponent;

  
  // ? Time buttons
  @ViewChild('oneHourButtonRef') oneHourButtonRef: CustomButtonComponent;
  @ViewChild('sixHoursButtonRef') sixHoursButtonRef: CustomButtonComponent;
  @ViewChild('twelveHoursButtonRef') twelveHoursButtonRef: CustomButtonComponent;
  @ViewChild('oneDayButtonRef') oneDayButtonRef: CustomButtonComponent;
  @ViewChild('predictionButtonRef') predictionButtonRef: CustomButtonComponent;

  // ? Statistic buttons
  @ViewChild('emaButtonRef') emaButtonRef: CustomButtonComponent;
  @ViewChild('smaButtonRef') smaButtonRef: CustomButtonComponent;
  @ViewChild('bollingerButtonRef') bollingerButtonRef: CustomButtonComponent;

  @Output() closeRequest = new EventEmitter<void>();

  constructor(private loginHelper: LoginHelperService,
      private loginApiHelper: LoginApiService,
      private generalHelper: GeneralMethodsService,
      private intersectionDataGetterAPI: IntersectionDataGetterService) { 
    Object.assign(this, { multi });
    this.init_button_configs();
  }

  ngOnInit(): void {
    // this.request_check_available_ranges();
    //this.request_get_intersection_data_stream();
  }

	// * PUBLIC METHODS --------------------------------------------------------

	public set_current_arm(currentArm: Arms) {
        if(currentArm.originTsc == this.originTsc && 
                currentArm.nextTsc == this.destTsc) {
            this.closeRequest.emit();
        } else {
            this.chartTitle = currentArm.streetName;
            this.originTsc = currentArm.originTsc;
            this.destTsc = currentArm.nextTsc;
            
            if(!currentArm.forecastAvailable) {
                this.predictionButtonRef.disable_button();
            } else {
                this.predictionButtonRef.enable_button();
            }

            // Set the intersection input data
            this.getIntersectionInputData = {
            tsc: currentArm.originTsc,
            timeRange: IntersectionTimeRange.ONE_HOUR
            }

            // Send off the request
            this.request_get_intersection_data_stream();

            this.set_one_hour_button();
        }
	}

	public submenu_state_change_handler($event: SubMenuState) {
		console.log($event);
        switch($event.submenuType) {
            case SubMenuTypes.EMA:
                this.handle_ema_state_change(
                        $event.data as SubMenu_EMA_StateData);
                break;
            case SubMenuTypes.SMA:
                this.handle_sma_state_change(
                        $event.data as SubMenu_SMA_StateData);
                break;
            case SubMenuTypes.BOLLINGER:
                this.handle_bollinger_state_change(
                        $event.data as SubMenu_BOLLINGER_StateData);
                break;
        }
	}

	public button_handler(type: string) {
		switch(type) {
		// ? Top button handlers
		case 'pred':
			console.log("Hello from prediction");
            this.getForecastInputData = {
                tsc: this.originTsc
            }
            this.request_get_forecast();
			break;
		case '1h':
			// 1 hour of data
			this.getIntersectionInputData.timeRange = IntersectionTimeRange.ONE_HOUR;
			this.request_get_intersection_data_stream();
			this.set_one_hour_button();
			break;
		case '6h':
			// 6 hours of data
			this.getIntersectionInputData.timeRange = IntersectionTimeRange.SIX_HOURS;
			this.request_get_intersection_data_stream();
			this.set_six_hour_button();
			break;
		case '12h':
			// 12 hours of data
			this.getIntersectionInputData.timeRange = IntersectionTimeRange.TWELVE_HOURS;
			this.request_get_intersection_data_stream();
			this.set_twelve_hour_button();
			break;
		case '1d':
			// 1 day of data
			this.getIntersectionInputData.timeRange = IntersectionTimeRange.ONE_DAY;
			this.request_get_intersection_data_stream();
			this.set_one_day_button();
			break;
		// ? Bottom button handlers
		case 'ema':
            this.handle_ema_button_push();
			break;
		case 'sma':
			// Simple moving average
            this.handle_sma_button_push();
			break;
		case 'boli':
            this.handle_bollinger_button_push();
			// Bolliger index
			break;
		case 'close':
			// Close
            this.closeRequest.emit();
			console.log("Hello from close");
			break;
		default:
			console.error("Unknown button type: " + type);
		}
	}

	public on_select($event) {
		// console.log("Select Event:");
		// console.log($event)
	}

	public on_activate($event) {
		// console.log("Active Event:");
		// console.log($event);
	}

	public on_deactivate($event){
		// console.log("Deactivate Event:");
		// console.log($event);
	}

  	// * PUBLIC VARIABLES ------------------------------------------------------

	// ? Chart title
	public chartTitle: string = "Default Title";
	public originTsc: number = 0;
	public destTsc: number = 0;

	// ? Chart config options
	public legend: boolean = true;
	public showLabels: boolean = false;
	public animations: boolean = false;
	public xAxis: boolean = false;
	public yAxis: boolean = true;
	public showXAxisLabel: boolean = true;
	public showYAxisLabel: boolean = true;
	public xAxisLabel: string = 'Time';
	public yAxisLabel: string = 'Traffic Volume (Cars through intersection)';
	public timeline: boolean = false;

	public multi: IntersectionData[];
	public view: any[] = [800, 300];

	public showChartData: boolean = false;

    public chartColours: IntersectionColourContainer[] = [
        {
            name: 'EMA',
            value: '#00a8ff'
        },
        {
            name: 'Raw Data',
            value: '#ee5253'
        },
        {
            name: 'SMA',
            value: '#009432'
        },
        {
            name: 'BOLU',
            value: '#9980FA'
        },
        {
            name: 'BOLD',
            value: '#ffa801'
        },
        {
            name: 'Forecast',
            value: '#7986cb'
        }
    ]

	public chartData: IntersectionData = {
		name: 'Raw data',
		series: []
	};

	public chartLoadingMessage: string = '';

	public chartLoadingSpinnerConfig: ProgressSpinnerConfig = {
		colour: 'accent',
		diameter: 100,
		mode: 'determinate',
		value: 0
	}

    public predictionSpinnerConfig: ProgressSpinnerConfig = {
		colour: 'accent',
		diameter: 40,
		mode: 'indeterminate',
		value: 0
	}

    public showPredictionButton: boolean = true;

	// ? List of submenus
	public listOfSubmenus: SubMenuState[] = [];

	// ? Top Buttons
	public predictionButtonConfig: CustomButtonConfig;
	public oneHourButtonConfig: CustomButtonConfig;
	public sixHoursButtonConfig: CustomButtonConfig;
	public twelveHoursButtonConfig: CustomButtonConfig;
	public dayButtonConfig: CustomButtonConfig;

	// ? Bottom Buttons
	public EMAButtonConfig: CustomButtonConfig;
	public SMAButtonConfig: CustomButtonConfig;
	public BOLIButtonConfig: CustomButtonConfig;
	public closeButtonConfig: CustomButtonConfig;

    // ? Forecast enable
    public forecastEnabled: boolean = false;

  	// * PRIVATE METHODS -------------------------------------------------------

    private clear_all_submenu_items() {
        // Clear EMA
        this.emaButtonRef.set_has_clicked(false);
        this.emaOn = false;
        this.emaState = {
            periodNumber: 3,
            smoothingFactor: 2
        }

        // Clear SMA
        this.smaButtonRef.set_has_clicked(false);
        this.smaOn = false;
        this.smaState = {
            periodNumber: 3
        }

        // Clear Bollinger
        this.bollingerButtonRef.set_has_clicked(false);
        this.bollingerOn = false;
        this.bollingerState = {
            movingAverageType: BollingerMAType.SMA,
            periodNumber: 3,
            smoothingFactor: 2,
            stdevNumber: 2
        }

        this.listOfSubmenus = [];

        this.forecastData.series = [];
    }

    /**
     * Handles the EMA button pushes (i.e. adding and removing the EMA submenu
     *  and EMA graph line)
     */
	private handle_ema_button_push() {
		if(this.emaOn) {
            // Set the button to 'off'
            this.emaButtonRef.set_has_clicked(false);

            // Remove the ema data from submenus list
            this.listOfSubmenus.splice(
                this.listOfSubmenus.indexOf(
                    this.listOfSubmenus.find(e => e.name == 'EMA')), 
                1)
                
            // Reset the ema state
            this.emaState = {
                periodNumber: 3,
                smoothingFactor: 2
            }

                    // Remove the ema data from the graph
            this.multi.splice(this.multi.indexOf(this.emaData), 1);

            this.multi = this.multi.concat([]);

			this.emaOn = false;
		} else {
            // Set the button to 'on'
			this.emaButtonRef.set_has_clicked(true);

            // Generate the ema from the list of data
            let data: IntersectionDataPair[] = [];
            if(this.forecastData != undefined) {
                data = data.concat(this.chartData.series, 
                        this.forecastData.series);
            } else {
                data = data.concat(this.chartData.series);
            }
			this.emaData = this.generate_ema_from_data('EMA', 
					data);

			// Add the emaData to the list of displayed lines
			this.multi.push(this.emaData);
			
			// Force an update the of the ngx-charts module (pointer 
			//	reference change)
			this.multi = this.multi.concat([]);

			// Push a control submenu to the list of submenu's
			this.listOfSubmenus.push({
				data: this.emaState,
				name: 'EMA',
				submenuType: SubMenuTypes.EMA
			});

            this.emaOn = true;
		}	
	}

    private handle_ema_state_change(emaStateChange: SubMenu_EMA_StateData) {
        // Overwrite the emaState data
        this.emaState = emaStateChange;

        // Regenerate the EMA data from the available input data
        let data: IntersectionDataPair[] = [];
        if(this.forecastData != undefined) {
            data = data.concat(this.chartData.series, 
                    this.forecastData.series);
        } else {
            data = data.concat(this.chartData.series);
        }
        const temp = this.generate_ema_from_data('EMA', data);
        
        // Remove the old ema data from the multi list and replace it with 
        //  the new data.
        this.multi.splice(this.multi.indexOf(this.multi.find(e => e.name == 'EMA')), 1, temp);

        // Save this new data to a class variable.
        this.emaData = temp;

        // Force update of the ngx-charts graph
        this.multi = this.multi.concat([]);
    }

	/**
	 * Generates an Exponential moving average (EMA) from the current data set.
	 */
  	private generate_ema_from_data(name: string, data: IntersectionDataPair[]) {
		// Create a new EMA dataset
		this.emaData = {
			name: name,
			series: [data[0]]
		}

		// Compute a simple average to get started
		let emaYesterday = data[0].value;

		for(let i = 1; i < data.length; i++) {
			// Compute the current ema
			const commonPart = (this.emaState.smoothingFactor/
					(1 + this.emaState.periodNumber))
			
			const ema = data[i].value * commonPart + 
				emaYesterday * (1 - commonPart);
			
			// Add the ema to the data series
			this.emaData.series.push({
				name: data[i].name,
				value: ema
			});

			// Save our recently computed ema
			emaYesterday = ema;
		}
		
		return this.emaData;
	}

    private handle_sma_button_push() {
        if(this.smaOn) {
            // Set the button to 'off'
            this.smaButtonRef.set_has_clicked(false);

            // Remove the sma data from submenus list
            this.listOfSubmenus.splice(
                this.listOfSubmenus.indexOf(
                    this.listOfSubmenus.find(e => e.name == 'SMA')), 
                1)
                
            // Reset the sma state
            this.smaState = {
                periodNumber: 3
            }

            // Remove the sma data from the graph
            this.multi.splice(
                    this.multi.indexOf(
                            this.multi.find(e => e.name == 'SMA')), 1);

            this.multi = this.multi.concat([]);
            this.smaOn = false;
        } else {
            // Set the button to 'on'
			this.smaButtonRef.set_has_clicked(true);
            
            let data: IntersectionDataPair[] = [];
            if(this.forecastData != undefined) {
                data = data.concat(this.chartData.series, this.forecastData.series);
            } else {
                data = data.concat(this.chartData.series);
            }

            // Generate the ema from the list of data
			this.smaData = this.generate_sma_from_data('SMA', 
                    data);

			// Add the emaData to the list of displayed lines
			this.multi.push(this.smaData);
			
			// Force an update the of the ngx-charts module (pointer 
			//	reference change)
			this.multi = this.multi.concat([]);

			// Push a control submenu to the list of submenu's
			this.listOfSubmenus.push({
				data: this.smaState,
				name: 'SMA',
				submenuType: SubMenuTypes.SMA
			});

            this.smaOn = true;
        }
    }

    private handle_sma_state_change(smaStateChange: SubMenu_SMA_StateData) {
        // Overwrite the smaState data
        this.smaState = smaStateChange;

        // Regenerate the SMA data from the available input data
        let data: IntersectionDataPair[] = [];
        if(this.forecastData != undefined) {
            data = data.concat(this.chartData.series, 
                    this.forecastData.series);
        } else {
            data = data.concat(this.chartData.series);
        }
        const temp = this.generate_sma_from_data('SMA', data);

        // Remove the old SMA data from the multi list and replace it with 
        //  the new data.
        this.multi.splice(
                this.multi.indexOf(
                        this.multi.find(e => e.name == 'SMA')), 1, temp);
        
        // Save the newly generated sma data to a class variable
        this.smaData = temp;

        // Force an update of the ngx-chart module
        this.multi = this.multi.concat([]);
    }

    /**
     * Generates a Simple Moving Average (SMA) from the current data set.
     */
    private generate_sma_from_data(name: string, data: IntersectionDataPair[]) {
        let smaSum: number = 0;

        const periodNum = this.smaState.periodNumber;

        // Perform the initial setup
        for(let i = 0; i < periodNum; i++) {
            smaSum += data[i].value;
        }

        this.smaData = {
            name: name,
            series: [{
                name: data[periodNum - 1].name,
                value: smaSum / periodNum
            }]
        }

        // Now perform the rest
        for(let i = periodNum; i < data.length; i++) {
            smaSum = 0;
            for(let j = i - periodNum; j < i; j++) {
                smaSum += data[j].value;
            }
            this.smaData.series.push({
                name: data[i].name,
                value: smaSum / periodNum
            });
        }

        return this.smaData;

    }

    private handle_bollinger_button_push() {
        if(this.bollingerOn) {
            // Remove the bollinger bands
            this.multi.splice(this.multi.indexOf(this.boluData), 1);
            this.multi.splice(this.multi.indexOf(this.boldData), 1);
            
            // Remove the sub-menu
            this.listOfSubmenus.splice(
                    this.listOfSubmenus.indexOf(
                            this.listOfSubmenus.find(
                                    e => e.name == 'BOLLINGER')), 
                    1);

            // Reset the bollinger state
            this.bollingerState = {
                movingAverageType: BollingerMAType.SMA,
                periodNumber: 3,
                stdevNumber: 2,
                smoothingFactor: 2
            }

            // Force update of the ngx-charts module
            this.multi = this.multi.concat([]);
            
            // Turn the bollinger button 'off'
            this.bollingerButtonRef.set_has_clicked(false);

            this.bollingerOn = false;
        } else {
            // Generate and add the bollinger bands
            let data: IntersectionDataPair[] = [];
            if(this.forecastData != undefined) {
                data = data.concat(this.chartData.series, 
                        this.forecastData.series);
            } else {
                data = data.concat(this.chartData.series);
            }
            const bollingerData = this.generate_bollinger_bands_from_data(
                    'BOLLINGER', 
                    data);
            
            this.multi.push(bollingerData[0]); // BOLU
            this.multi.push(bollingerData[1]); // BOLD
            
            // Push a sub-menu
            this.listOfSubmenus.push({
				data: this.bollingerState,
				name: 'BOLLINGER',
				submenuType: SubMenuTypes.BOLLINGER
			});

            // Force an update of the ngx-charts module
            this.multi = this.multi.concat([]);

            // Turn the bollinger button 'on'
            this.bollingerButtonRef.set_has_clicked(true);

            this.bollingerOn = true;
        }
    }

    private handle_bollinger_state_change(
            bollingerStateChange: SubMenu_BOLLINGER_StateData) {
        // Overwrite the emaState data
        this.bollingerState = bollingerStateChange;

        // Regenerate the EMA data from the available input data
        let data: IntersectionDataPair[] = [];
        if(this.forecastData != undefined) {
            data = data.concat(this.chartData.series, 
                    this.forecastData.series);
        } else {
            data = data.concat(this.chartData.series);
        }
        const temp = this.generate_bollinger_bands_from_data('BOLLINGER', 
                data);
        
        // Remove the old ema data from the multi list and replace it with 
        //  the new data.
        this.multi.splice(
                this.multi.indexOf(
                        this.multi.find(e => e.name == 'BOLU')), 1, temp[0]);
        this.multi.splice(
                this.multi.indexOf(
                        this.multi.find(e => e.name == 'BOLD')), 1, temp[1]);
        

        // Save this new data to a class variable.
        this.boluData = temp[0];
        this.boldData = temp[1];

        // Force update of the ngx-charts graph
        this.multi = this.multi.concat([]);
    }

    /**
     * Generates the Bollinger bands from the current data set.
     */
    private generate_bollinger_bands_from_data(name: string, 
            data: IntersectionDataPair[]) {
        let emaYesterday = 0;
        let smaSum = 0;
        let dataSamples: number[] = [];
        let boluValue = 0;
        let boldValue = 0;
            
        // Constants
        const periodNum = this.bollingerState.periodNumber;

        // Perform the initial setup
        for(let i = 0; i < periodNum; i++) {
            smaSum += data[i].value;
            dataSamples.push(data[i].value);
        }
            
        // Compute the BOLU and BOLD values
        boluValue = smaSum / periodNum + 
                this.bollingerState.stdevNumber * 
                        this.compute_standard_deviation(dataSamples);
        
        boldValue = smaSum / periodNum - 
                this.bollingerState.stdevNumber * 
                        this.compute_standard_deviation(dataSamples);

        // Save these values to the list
        this.boluData = {
            name: 'BOLU',
            series: [{
                name: data[periodNum - 1].name,
                value: boluValue
            }]
        }

        this.boldData = {
            name: 'BOLD',
            series: [{
                name: data[periodNum - 1].name,
                value: boldValue
            }]
        }

        // We will use the SMA as the inital value for the emaYesterday value
        emaYesterday = smaSum / periodNum

        // Main loop
        for(let i=periodNum; i < data.length; i++) {
            // Clear the data samples
            dataSamples = [];
            if(this.bollingerState.movingAverageType == BollingerMAType.EMA) {
                
                for(let j = i - periodNum; j < i; j++) {
                    dataSamples.push(data[j].value);
                }

                const commonPart = (this.emaState.smoothingFactor/
					(1 + this.emaState.periodNumber))

                const ema = data[i].value * commonPart + 
                    emaYesterday * (1 - commonPart);

                boluValue = ema + this.bollingerState.stdevNumber * 
                            this.compute_standard_deviation(dataSamples);

                boldValue = ema - this.bollingerState.stdevNumber * 
                            this.compute_standard_deviation(dataSamples);

                emaYesterday = ema;
            } else {
                // Reset sum
                smaSum = 0;

                for(let j = i - periodNum; j < i; j++) {
                    smaSum += data[j].value;
                    dataSamples.push(data[j].value);
                }

                // Compute the BOLU and BOLD values
                boluValue = smaSum / periodNum + 
                        this.bollingerState.stdevNumber * 
                                this.compute_standard_deviation(dataSamples);

                boldValue = smaSum / periodNum - 
                        this.bollingerState.stdevNumber * 
                                this.compute_standard_deviation(dataSamples);
            }

            // Save the bollinger data
            this.boluData.series.push({
                name: data[i].name,
                value: boluValue
            });
            
            this.boldData.series.push({
                name: data[i].name,
                value: boldValue
            });
        }
        
        // Return the data as a list (BOLU, BOLD)
        return [this.boluData, this.boldData]
    }

    /**
     * Computes the standard deviation of a sequence of numbers.
     * @param data 
     */
    private compute_standard_deviation(data: number[]) {
        const mean = data.reduce((s, n) => s + n) / data.length;
        const variance = data.reduce((s, n) => s + (n - mean) ** 2, 0) / 
                (data.length - 1);
        return Math.sqrt(variance);
    }

    private set_one_hour_button() {
        this.oneHourButtonRef.set_has_clicked(true);
        this.sixHoursButtonRef.set_has_clicked(false);
        this.twelveHoursButtonRef.set_has_clicked(false);
        this.oneDayButtonRef.set_has_clicked(false);
    }

    private set_six_hour_button() {
        this.oneHourButtonRef.set_has_clicked(false);
        this.sixHoursButtonRef.set_has_clicked(true);
        this.twelveHoursButtonRef.set_has_clicked(false);
        this.oneDayButtonRef.set_has_clicked(false);
    }

    private set_twelve_hour_button() {
        this.oneHourButtonRef.set_has_clicked(false);
        this.sixHoursButtonRef.set_has_clicked(false);
        this.twelveHoursButtonRef.set_has_clicked(true);
        this.oneDayButtonRef.set_has_clicked(false);
    }

    private set_one_day_button() {
        this.oneHourButtonRef.set_has_clicked(false);
        this.sixHoursButtonRef.set_has_clicked(false);
        this.twelveHoursButtonRef.set_has_clicked(false);
        this.oneDayButtonRef.set_has_clicked(true);
    }

    /**
     * Disables pointer events for the buttons in the data displayer
     */
    private disable_buttons() {
        this.oneHourButtonRef.disable_button();
        this.sixHoursButtonRef.disable_button();
        this.twelveHoursButtonRef.disable_button();
        this.oneDayButtonRef.disable_button();
    }

    /**
     * Enables pointer events for the buttons in the data displayer
     */
    private enable_buttons() {
        this.oneHourButtonRef.enable_button();
        this.sixHoursButtonRef.enable_button();
        this.twelveHoursButtonRef.enable_button();
        this.oneDayButtonRef.enable_button();
    }

    /**
     * Checks the `chartData` variable for missing entries.
     * If entries are missing, the method will linearly interpolate between them,
     *  or repeat the sequence (if nothing is available to interpolate to).
     */
    private check_and_interpolate() {
        // 5 minutes in milliseconds
        this.chartData.series = this.chartData.series.reverse();
        const timeStep: number = 5 * 60 * 1000
        let temp: IntersectionDataPair[] = [this.chartData.series[0]];

        for(let i = 1; i < this.chartData.series.length; i++) {
        const t1 = new Date(this.chartData.series[i - 1].name);
        const t2 = new Date(this.chartData.series[i].name);

        const steps = (t2.getTime() - t1.getTime())/timeStep;
        // console.log(steps)
        
        if(steps > 1) {
            // console.log("Missing entry, interpolating between:")
            // console.log("t1: " + t1.toLocaleString() + " -> " + "t2: " + t2.toLocaleString())
            const gradient = (this.chartData.series[i].value - 
                this.chartData.series[i-1].value)/(steps);

            // We need to insert some entries
            for(let j = 1; j < steps; j++) {
            const newCmf = this.chartData.series[i-1].value + gradient * j;
            const newTime = t1.getTime() + timeStep * j;
            temp.push({
                name: (new Date(newTime)).toLocaleString(),
                value: newCmf
            });
            }
        }
        // Make sure we add the next step
        temp.push(this.chartData.series[i]);
        }

        this.chartData.series = temp;
    }

    private init_button_configs() {
        const baseButtonConfig: CustomButtonConfig = {
        buttonType: CustomButtonType.REGULAR_BUTTON,
        height: 40,
        scale: undefined,
        width: 100,
        toggle: false,
        toggleClass: 'button-flat-colour-on',
        icon: "",
        regularButtonIconEnable: true,
        isDisabled: false
        };

        // ? Top buttons
        const topButtonsWidth = 40;
        const bottomButtonsWidth = 60;

        // Prediction button
        this.predictionButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.predictionButtonConfig.buttonType = CustomButtonType.ICON_BUTTON_W_BG;
        this.predictionButtonConfig.width = topButtonsWidth;
        this.predictionButtonConfig.icon = 'tips_and_updates';
        this.predictionButtonConfig.isDisabled = false;
        
        // 1h
        this.oneHourButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.oneHourButtonConfig.regularButtonIconEnable = false;
        this.oneHourButtonConfig.width = topButtonsWidth;

        // 6h
        this.sixHoursButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.sixHoursButtonConfig.regularButtonIconEnable = false;
        this.sixHoursButtonConfig.width = topButtonsWidth;

        // 12h
        this.twelveHoursButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.twelveHoursButtonConfig.regularButtonIconEnable = false;
        this.twelveHoursButtonConfig.width = topButtonsWidth;

        // 1d
        this.dayButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.dayButtonConfig.regularButtonIconEnable = false;
        this.dayButtonConfig.width = topButtonsWidth;

        // ? Bottom buttons

        // EMA
        this.EMAButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.EMAButtonConfig.regularButtonIconEnable = false;
        this.EMAButtonConfig.width = bottomButtonsWidth;

        // SMA
        this.SMAButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.SMAButtonConfig.regularButtonIconEnable = false;
        this.SMAButtonConfig.width = bottomButtonsWidth;

        // BOLI
        this.BOLIButtonConfig = this.generalHelper.clone_object(
        baseButtonConfig);
        this.BOLIButtonConfig.regularButtonIconEnable = false;
        this.BOLIButtonConfig.width = bottomButtonsWidth;

        // Close button
        this.closeButtonConfig = this.generalHelper.clone_object(
                baseButtonConfig);
        this.closeButtonConfig.icon = 'close';
    }

  	// * PRIVATE VARIABLES -----------------------------------------------------

	// ? Analysis metrics
	private emaData: IntersectionData;
	private emaState: SubMenu_EMA_StateData = {
		periodNumber: 3,
		smoothingFactor: 2
	}
	private emaOn: boolean = false;

	private smaData: IntersectionData;
    private smaState: SubMenu_SMA_StateData = {
        periodNumber: 3
    }
	private smaOn: boolean = false;

	private boluData: IntersectionData;
	private boldData: IntersectionData;
    private bollingerState: SubMenu_BOLLINGER_StateData = {
        movingAverageType: BollingerMAType.SMA,
        periodNumber: 3,
        stdevNumber: 2,
        smoothingFactor: 2
    }
	private bollingerOn: boolean = false;

	private timeoutId;
	private errorDebouncer: boolean = false;
	private dataFramesLoaded: number = 0;

    // ? Forecast container
    private forecastData: IntersectionData = {
        name: 'Forecast',
        series: []
    }

  // ? CHECK AVAILABLE RANGES API ----------------------------------------------

  public request_check_available_ranges(): void {
    console.log("Attempting to get available ranges")
    this.intersectionDataGetterAPI.check_available_ranges(this);
  }

  public check_available_ranges_success_callback(data: number): void {
      console.log("Received data")
      const convertedData: IntersectionTimeRange = data;
      console.log(convertedData);
  }

  public check_available_ranges_unsuccessful_callback(errMsg: string): void {
      console.log("Unsuccessful in attempting to get available ranges");
  }

  public check_available_ranges_error_callback(err: any): void {
    if(this.loginHelper.check_jwt_expired_error(err)) {
      // Keep track of how many retries this is consuming
      this.checkAvailableRangesNumRetries++;
      if(this.checkAvailableRangesNumRetries > 5) {
        return;
      }

      // Go ahead with the retry
      this.loginApiHelper.send_refresh_jwt_request(this, 
          this.checkAvailableRangesApiMasterId);
    } else {
      console.error(err);
    }
  }

  public checkAvailableRangesApiMasterId: ApiIdMaster = ApiIdMaster
      .CHECK_AVAILABLE_RANGES;
  public checkAvailableRangesNumRetries: number = 0;

  // ? GET INTERSECTION DATA ---------------------------------------------------

  public request_get_intersection_data_stream(): void {
    this.dataFramesLoaded = 0;
    
    // Set the one hour button to highlight, and disable clicks
    this.disable_buttons();

    this.showChartData = false;
    
    this.chartData.series = [];

    // Clear all the submenu items currently active
    this.clear_all_submenu_items();

    console.log("Getting stream data...");
    this.intersectionDataGetterAPI.get_intersection_data_stream(this,
        this.getIntersectionInputData);
  }

  public get_intersection_data_on_data_callback(data: string): void {
    console.log("Data Received:");
    const convertedData = JSON.parse(data['array'][1]);
    console.log(convertedData);
    for(let i = 0; i < convertedData.length; i++) {
     
      const dateObject = new Date(convertedData[i]['timestamp']);

      this.chartData.series.push({
        name: dateObject.toLocaleString(),
        value: convertedData[i]['cmf']
      });
    }

    this.dataFramesLoaded++;

    // Compute the percentage value
    const loadingPercentage: number = Math.round(this.dataFramesLoaded / 
        data['array'][2]*10000)/100;


    // Update the chart loading spinner
    this.loadingSpinnerRef.set_spinner_value(loadingPercentage)

    // Update the chart loading message
    this.chartLoadingMessage = 'Loading data... ' + loadingPercentage + '%';

  }

  public get_intersection_data_on_status_callback(status: any): void {
      console.log("Received status:");
      console.log(status);
      this.enable_buttons();
  }

  public get_intersection_data_on_error_callback(errMsg: string): void {
    console.log("Received error:");
    if(!this.errorDebouncer && 
            this.loginHelper.check_jwt_expired_error(errMsg)) {
        // Handle debouncing
        this.errorDebouncer = true;
        this.timeoutId = setTimeout(() => {
            this.errorDebouncer = false;
        }, 500);

        this.getIntersectionDataStreamNumRetries++;
        // Keep track of how many retries this is consuming.
        if(this.getIntersectionDataStreamNumRetries > 5) {
            return;
        }

        console.log('refreshing jwt');

        // Go ahead with the retry.
        this.loginApiHelper.send_refresh_jwt_request(this, 
                this.getIntersectionDataStreamApiMasterId);
    } else {
        console.error(errMsg);
    }
    this.enable_buttons();
  }

  public get_intersection_data_on_end_callback(): void {
      console.log("Stream has ended");
      console.log(this.chartData);
      // Check and interpolate (if required)
      this.check_and_interpolate()
      this.multi = [this.chartData];
      this.showChartData = true;
      this.enable_buttons();
  }

  public getIntersectionDataStreamApiMasterId: ApiIdMaster = 
        ApiIdMaster.GET_INTERSECTION_DATA;
  public getIntersectionDataStreamNumRetries: number = 0;
  public getIntersectionInputData: IntersectionDataGetterRequestContainer;

  // ? GET FORECAST API --------------------------------------------------------

  public request_get_forecast(): void {
      // Clear the previous forecast data
      this.forecastData.series = [];

      // Hide the prediction button (show the spinner)
      this.showPredictionButton = false;
      this.intersectionDataGetterAPI.get_forecast(this, 
            this.getForecastInputData);
  }

  public get_forecast_on_data_callback(data: string): void {
    console.log("We got data!");
    const convertedData = JSON.parse(data['array'][1]);
    console.log(convertedData);
    
    // Get the latest timestamp from the data set
    for(let i = 0; i < convertedData[0].length; i++) {
        const dateObject = new Date(convertedData[1][i]);
        this.forecastData.series.push({
            name: dateObject.toLocaleString(),
            value: convertedData[0][i]
        });
    }
  }

  public get_forecast_on_error_callback(errMsg: string): void {
    if(!this.errorDebouncer && 
            this.loginHelper.check_jwt_expired_error(errMsg)) {
        // Handle debouncing
        this.errorDebouncer = true;
        this.timeoutId = setTimeout(() => {
            this.errorDebouncer = false;
        }, 500);

        this.getForecastNumNumRetries++;
        // Keep track of how many retries this is consuming.
        if(this.getForecastNumNumRetries > 5) {
            return;
        }

        console.log('refreshing jwt');

        // Go ahead with the retry.
        this.loginApiHelper.send_refresh_jwt_request(this, 
                this.getForecastApiMasterId);
    } else {
        console.error(errMsg);
    }

    // Show the prediction button again (hide the spinner)
    this.showPredictionButton = true;
  }

  public get_forecast_on_status_callback(status: any): void {
    console.log("We got a status");
    console.log(status);
  }

  public get_forecast_on_end_callback(): void {
    // Check if we need to remove the previous forecast
    const forecastData = this.multi.find(e => e.name == 'Forecast');

    if(forecastData != undefined) {
        this.multi.splice(this.multi.indexOf(forecastData), 1);
    }
    
    // Check if we need to update other indexes.
    if(this.emaData != undefined) {
        // Remove the ema data and regenerate it
        this.handle_ema_button_push();
        this.handle_ema_button_push();
    }

    if(this.smaData != undefined) {
        // Remove the sma data and regenerate it
        this.handle_sma_button_push();
        this.handle_sma_button_push();
    }

    if(this.boldData != undefined || this.boluData != undefined) {
        // Remove the bold data and regenerate it
        this.handle_bollinger_button_push();
        this.handle_bollinger_button_push();
    }

    this.multi.push(this.forecastData);
    this.multi = this.multi.concat([]);
    this.showPredictionButton = true;
    
  }

  public getForecastApiMasterId: ApiIdMaster = ApiIdMaster.GET_FORECAST;
  public getForecastNumNumRetries: number = 0;
  public getForecastInputData: GetForecastRequestContainer;

}
