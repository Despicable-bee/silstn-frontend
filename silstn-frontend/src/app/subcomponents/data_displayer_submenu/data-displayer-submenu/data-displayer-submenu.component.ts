// ANGULAR ---------------------------------------------------------------------

import { 
  Component, 
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  ChangeDetectorRef
} from '@angular/core';

import {
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

// INTERFACES ------------------------------------------------------------------

// APIS ------------------------------------------------------------------------

// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

import {
    BollingerMAType,
    SubMenuTypes
} from 'src/app/data_structures/shared/data_displayer_submenu/data_displayer_submenu.ds';

// DATA STRUCTURES -------------------------------------------------------------

import {
    SubMenuState,
    SubMenu_BOLLINGER_StateData,
    SubMenu_EMA_StateData,
    SubMenu_SMA_StateData
} from 'src/app/data_structures/shared/data_displayer_submenu/data_displayer_submenu.ds';

@Component({
    selector: 'app-data-displayer-submenu',
    templateUrl: './data-displayer-submenu.component.html',
    styleUrls: ['./data-displayer-submenu.component.scss']
})
export class DataDisplayerSubmenuComponent implements OnInit {

    @Input() subMenuInput: SubMenuState;
    @Output() subMenuStateChange = new EventEmitter<SubMenuState>();

    constructor(private formBuilder: FormBuilder,
            private cd: ChangeDetectorRef) { }

    ngOnInit(): void {
        if(this.subMenuInput != undefined) {
            this.set_submenu_state(this.subMenuInput);
        }
    }

    // * PUBLIC METHODS --------------------------------------------------------

    public input_change_handler(type: string) {
        switch(type) {
            case 'ema':
                this.ema_change_handler();
                break;
            case 'sma':
                this.sma_change_handler();
                break;
            case 'bollinger':
                this.bollinger_change_handler();
                break;
            default:
        }
    }

    // * PUBLIC VARIABLES ------------------------------------------------------

    public stateName: string;

    // ? EMA controllable variables
    public isTypeEMA: boolean = false;
    public emaSmoothingFactor: number = 2;
    public emaSmoothingMax: number = 5;
    public emaSmoothingMin: number = 1;
    public emaSmoothingStep: number = 1;
    public emaSmoothingShowThumblabel: boolean = true;

    public emaNumPeriods: number = 3;
    public emaNumPeriodsMax: number = 15;
    public emaNumPeriodsMin: number = 2;
    public emaNumPeriodsStep: number = 1;
    public emaNumPeriodsShowThumblabel: boolean = true;

    // ? SMA controllable variables
    public isTypeSMA: boolean = false;
    public smaNumPeriods: number = 3;
    public smaNumPeriodsMax: number = 15;
    public smaNumPeriodsMin: number = 2;
    public smaNumPeriodsStep: number = 1;
    public smaNumPeriodsShowThumblabel: boolean = true;

    // ? Bollinger controllable variables
    public isTypeBollinger: boolean = false;
    public bollingerMAType: BollingerMAType = BollingerMAType.SMA;
    // ! NOTE - The bollinger bands mode borrows from the EMA and SMA 
    // !    controllable variables, so don't fret over why they aren't specified
    // !    explicitly here.

    public bollingerStdevs: number = 2;
    public bollingerStdevsMax: number = 4;
    public bollingerStdevsMin: number = 1;
    public bollingerStdevsStep: number = 0.2;
    public bollingerStdevsShowThumbLabel: boolean = true;
    
    // ? Bollinger form controls
    public MATypeFormGroup: FormGroup;

    // * PRIVATE METHODS -------------------------------------------------------

    private set_submenu_state(newState: SubMenuState) {
        this.stateName = newState.name;
        switch(newState.submenuType) {
            case SubMenuTypes.EMA:
                this.isTypeEMA = true;
                this.ema_set_state(newState.data as SubMenu_EMA_StateData);
                break;
            case SubMenuTypes.SMA:
                this.sma_set_state(newState.data as SubMenu_SMA_StateData);
                break;
            case SubMenuTypes.BOLLINGER:
                this.bollinger_set_state(
                        newState.data as SubMenu_BOLLINGER_StateData);
                break;
            default:
                console.error("Unknown state type: " + newState.submenuType);
        }
    }

    private ema_change_handler() {
        this.subMenuStateChange.emit({
            data: {
                smoothingFactor: this.emaSmoothingFactor,
                periodNumber: this.emaNumPeriods
            },
            name: this.stateName,
            submenuType: SubMenuTypes.EMA
        });
    }

    private bollinger_change_handler() {
        // Determine what the moving average type is
        this.cd.detectChanges();
        const maType = this.MATypeFormGroup.get('movingAverageType').value;
        console.log(maType);
        
        let maTypeVerified: BollingerMAType = BollingerMAType.SMA;

        switch(maType) {
            case 'sma':
                maTypeVerified = BollingerMAType.SMA;
                break;
            case 'ema':
                maTypeVerified = BollingerMAType.EMA;
                break;
            default:
                console.error("Unknown moving average type: " + maType);
        }


        // Emit
        this.subMenuStateChange.emit({
            submenuType: SubMenuTypes.BOLLINGER,
            data: {
                movingAverageType: maTypeVerified,
                periodNumber: this.smaNumPeriods,
                smoothingFactor: this.emaSmoothingFactor,
                stdevNumber: this.bollingerStdevs 
            },
            name: this.stateName
        });
    }

    private sma_change_handler() {
        this.subMenuStateChange.emit({
            submenuType: SubMenuTypes.SMA,
            data: {
                periodNumber: this.smaNumPeriods
            },
            name: this.stateName
        })
    }

    private ema_set_state(emaStateData: SubMenu_EMA_StateData) {
        this.emaSmoothingFactor = emaStateData.smoothingFactor;
        this.emaNumPeriods = emaStateData.periodNumber;
        this.isTypeEMA = true;
    }

    private sma_set_state(smaStateData: SubMenu_SMA_StateData) {
        this.smaNumPeriods = smaStateData.periodNumber;
        this.isTypeSMA = true;
    }

    private bollinger_set_state(bollingerStateData: SubMenu_BOLLINGER_StateData) {
        this.bollingerStdevs = bollingerStateData.stdevNumber;
        if(bollingerStateData.movingAverageType == BollingerMAType.SMA) {
            this.smaNumPeriods = bollingerStateData.periodNumber;
        } else {
            this.emaNumPeriods = bollingerStateData.periodNumber;
            this.emaSmoothingFactor = bollingerStateData.smoothingFactor;
        }
        this.init_bollinger_form();
        this.isTypeBollinger = true;
    }

    private init_bollinger_form() {
        this.MATypeFormGroup = this.formBuilder.group({
            movingAverageType: ['sma', Validators.required]
        });

        this.MATypeFormGroup.get('movingAverageType').valueChanges.subscribe(
            () => {
                this.bollinger_change_handler();
            }
        )
    }

    // * PRIVATE VARIABLES -----------------------------------------------------

}
