import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDisplayerSubmenuComponent } from './data-displayer-submenu.component';

describe('DataDisplayerSubmenuComponent', () => {
  let component: DataDisplayerSubmenuComponent;
  let fixture: ComponentFixture<DataDisplayerSubmenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDisplayerSubmenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDisplayerSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
