// ANGULAR ---------------------------------------------------------------------

import { 
  Component, 
  OnInit,
  Input
} from '@angular/core';

import {
  ThemePalette
} from '@angular/material/core';

import {
  ProgressSpinnerMode
} from '@angular/material/progress-spinner';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

// INTERFACES ------------------------------------------------------------------

// APIS ------------------------------------------------------------------------

// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

// DATA STRUCTURES -------------------------------------------------------------

import {
  ProgressSpinnerConfig
} from 'src/app/data_structures/shared/loading-spinner/loading-spinner.ds';

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.scss']
})
export class LoadingSpinnerComponent implements OnInit {

  @Input() progressSpinnerConfig: ProgressSpinnerConfig;

  constructor() {
  }

  ngOnInit(): void {
    this.spinnerValue = this.progressSpinnerConfig.value;
    this.spinnerColour = this.progressSpinnerConfig.colour;
    this.spinnerMode = this.progressSpinnerConfig.mode;
    this.spinnerDiameter = this.progressSpinnerConfig.diameter;
  }

  // * PUBLIC METHODS ----------------------------------------------------------

  public set_spinner_value(newValue: number) {
    this.spinnerValue = newValue;
  }

  public set_spinner_colour(newColour: ThemePalette) {
    this.spinnerColour = newColour;
  } 

  public set_spinner_mode(newMode: ProgressSpinnerMode) {
    this.spinnerMode = newMode;
  }

  public set_spinner_diameter(newDiameter: number) {
    this.spinnerDiameter = newDiameter;
  }

  // * PUBLIC VARIABLES --------------------------------------------------------

  public spinnerMode: ProgressSpinnerMode = 'indeterminate';
  public spinnerColour: ThemePalette = "accent";
  public spinnerValue: number = 50;

  public spinnerDiameter: number = 30;

  // * PRIVATE METHODS ---------------------------------------------------------

  // * PRIVATE VARIABLES -------------------------------------------------------

}

