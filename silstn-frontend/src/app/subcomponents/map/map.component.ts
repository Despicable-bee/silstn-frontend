// ANGULAR ---------------------------------------------------------------------

import { 
    Component, 
    OnInit,
    Inject,
    EventEmitter,
    Output,
    ViewChild
} from '@angular/core';

import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
    LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

import {
    GeneralMethodsService
} from 'src/app/services/helper_methods/shared/general_methods/general-methods.service';

// INTERFACES ------------------------------------------------------------------

import {
    ChunkGetter,
    ChunkGetterStream
} from 'src/app/services/api_calls/specific/chunk_getter/chunk_getter.interface';

import {
    ApiIdMaster
} from 'src/app/services/api_id_master.ds';

// APIS ------------------------------------------------------------------------

import {
    LoginApiService
} from 'src/app/services/api_calls/specific/login_api/login-api.service';

import {
    ChunkGetterService
} from 'src/app/services/api_calls/specific/chunk_getter/chunk-getter.service';

// COMPONENTS ------------------------------------------------------------------

import {
    CustomButtonComponent
} from 'src/app/subcomponents/custom-button/custom-button.component';


// ENUMS -----------------------------------------------------------------------

import {
    CustomButtonClickEvent,
    CustomButtonType
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';


// DATA STRUCTURES -------------------------------------------------------------

import {
    Viewport
} from 'src/app/data_structures/specific/chunk-getter/chunk-getter.ds';

import {
    CustomButtonConfig
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';


import {
  LatLng,
  Chunk,
  LatLngLiteral,
  ChunkType,
  Intersection,
  Arms,
  Route,
  Step
} from 'src/app/data_structures/shared/agm-map/agm_map.ds';

import {
    testDataTriangles
} from './test_data';
import { ProgressSpinnerConfig } from 'src/app/data_structures/shared/loading-spinner/loading-spinner.ds';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, ChunkGetterStream {

    @Output() routeClickEvent = new EventEmitter<Arms>();

    @ViewChild('refreshButtonRef') refreshButtonRef: CustomButtonComponent;

    constructor(private loginHelper: LoginHelperService,
            private loginApiHelper: LoginApiService,
            private chunkGetterApiHelper: ChunkGetterService,
            private generalHelper: GeneralMethodsService,
            @Inject(PLATFORM_ID) platformId) { 
        this.isBrowser = isPlatformBrowser(platformId);
        if(this.isBrowser) {
            this.init_button_configs();
        }
    }

    ngOnInit(): void {
    }

    // * VIEWCHILD CALLABLE METHODS --------------------------------------------

    /**
     * Turns the debug menu on or off. 
     * - `True` == ON
     * - `False` == OFF
     * @param option The option specified by the user.
     */
    public set_debug_mode(option: boolean) {
        this.DEBUG_enabled = option;
    }

    // * PUBLIC METHODS --------------------------------------------------------

    public map_ready_handler($event) {
        // let trafficLayer = new google.maps.TrafficLayer();
    }

    /**
     * Clears the current clickly clicked polyline route.
     */
    public polyline_clear_click() {
        
        // Set the colour back to the default value
        if(this.previouslyClickedArm.forecastAvailable) {
            this.previouslyClickedArm.colour = this.forecastAvailableColour;
        } else {
            this.previouslyClickedArm.colour = this.standardColour;
        }
        

        // Clear the previously clicked variables
        this.previouslyClickedArm = undefined;
        this.previouslyClickedIntersec = undefined;
    }

    public polyline_mouse_over_handler(tsc: number, nextTsc: number) {
        let intersec = this.intersections.find( intersec => intersec.tsc == tsc);
        let arm = intersec.arms.find( arm => arm.nextTsc == nextTsc );
        arm.colour = this.roadHighlightColour;
    }

    public polyline_mouse_out_handler(tsc: number, nextTsc: number) {
        let intersec = this.intersections.find( intersec => intersec.tsc == tsc);
        let arm = intersec.arms.find( arm => arm.nextTsc == nextTsc );
        if(arm == undefined || arm != this.previouslyClickedArm) {
            if(intersec.forecastAvailable) {
                arm.colour = this.forecastAvailableColour;
            } else {
                arm.colour = this.standardColour;
            }
        } 
    }

    public polyline_mouse_click_handler(tsc: number, nextTsc: number) {
        console.log("Clicked on line: " + tsc);
        console.log("Clicked on nextTsc: " + nextTsc);

        // Get the arm we just clicked on
        let intersec = this.intersections.find( intersec => intersec.tsc == tsc);
        let arm = intersec.arms.find( arm => arm.nextTsc == nextTsc );
        
        if(this.previouslyClickedArm != undefined) {
            if(this.previouslyClickedArm == arm) {
                // Unclick
                if(this.previouslyClickedIntersec.forecastAvailable) {
                    this.previouslyClickedArm.colour = 
                            this.forecastAvailableColour;
                } else {
                    this.previouslyClickedArm.colour = this.standardColour;
                }
                this.previouslyClickedArm = undefined;
                this.previouslyClickedIntersec = undefined;
            } else {
                // Unclick previous and set new arm
                if(this.previouslyClickedIntersec.forecastAvailable) {
                    this.previouslyClickedArm.colour = 
                            this.forecastAvailableColour;
                } else {
                    this.previouslyClickedArm.colour = this.standardColour;
                }
                arm.colour = this.roadHighlightColour;

                this.previouslyClickedArm = arm;
                this.previouslyClickedIntersec = intersec;
            }            
        } else {
            // previously clicked is undefined, lets set the colour to red and 
            // set the previous variables
            arm.colour = this.roadHighlightColour;

            this.previouslyClickedArm = arm;
            this.previouslyClickedIntersec = intersec;
        }

        // Emit the arm
        this.routeClickEvent.emit(arm);
    }

    public button_handler(type: string) {
        switch(type) {
            case 'toggle':
                if(this.chunkLoaderEnabled) {
                    this.chunkLoaderEnabled = false;
                } else {
                    this.chunkLoaderEnabled = true;
                }
                break;
            case 'refresh':
                // Refresh the currently viewable chunks
                this.request_grab_chunks_stream();
                break;
            default:
                console.error("Unknown button type: " + type);
        }
    }

    public click_handler(triangle) {
        console.log(triangle)
    }

    public bounds_change_handler($event) {

        this.currentBounds = $event;
    }

    public zoom_change_handler($event) {
        this.currentZoomLevel = $event;
    }

    public idle_change_handler($event) {
        // Get the corner coordinates of the screen
        const topRight: LatLng = {
            latitude: this.currentBounds['Ab']['h'],
            longitude: this.currentBounds['Ra']['h'],
            show: undefined
        }

        const bottomLeft: LatLng = {
            latitude: this.currentBounds['Ab']['g'],
            longitude: this.currentBounds['Ra']['g'],
            show: undefined
        }

        this.DEBUG_llng = Math.round((bottomLeft.longitude)*10000)/10000;
        this.DEBUG_rLng = Math.round((topRight.longitude)*10000)/10000;

        const dimensions = this.compute_screen_metrics(topRight, 
                bottomLeft);
        this.compute_dimensions_units(dimensions[0], dimensions[1]);

        const currentChunkType = this.road_detail_check();
        // console.log(currentChunkType);
        // console.log(this.currentBounds);
        
        this.grabChunksStreamInputData = {
            maxLat: topRight.latitude,
            maxLng: topRight.longitude,
            minLat: bottomLeft.latitude,
            minLng: bottomLeft.longitude,
            chunkType: currentChunkType
        }
    }

    // * PUBLIC VARIABLES ------------------------------------------------------

    // ? The initial latitude and longitude coordinates to centre the camera 
    // ? around on init.
    public initialLatitude: number = -27.4975;
    public initialLongitude: number = 153.0137;

    // public initialLatitude: number = 18.000000000000004;
    // public initialLongitude: number = 58.282525588538995;

    // ? The minimum zoom the camera can have.
    public minimumZoom: number = 10;

    // ? Variables used in the Debug UI
    public DEBUG_enabled: boolean = true;

    public DEBUG_currentScreenWidth: number = 0;
    public DEBUG_currentScreenHeight: number = 0;

    public DEBUG_currentScreenWidthUnits: string = 'km';
    public DEBUG_currentScreenHeightUnits: string = 'km';

    public DEBUG_roadDetail: string = 'STANDARD';

    public DEBUG_currentComputingTask: string = "Idle";

    public DEBUG_numChunksLoaded: number = 0;

    public DEBUG_toggleChunkLoadingButtonConfig: CustomButtonConfig;

    public DEBUG_llng: number = 0.0;
    public DEBUG_rLng: number = 0.0;

    public currentZoomLevel: number = 8;

    public testTriangle: LatLngLiteral[][] = testDataTriangles;

    // ? Variable for enabling specific UI
    public isBrowser: boolean = false;

    public intersections: Intersection[] = [];

    public colorTest: string = 'red';

    // ? Loading spinner
    public chunkLoadingSpinnerConfig: ProgressSpinnerConfig = {
        colour: 'accent',
        diameter: 36,
        mode: 'indeterminate',
        value: 100
    };

    public showRefreshSpinner: boolean = false;

    // * PRIVATE METHODS -------------------------------------------------------

    /**
     * Computes a list of far chunk objects that are viewable on screen.
     * @param topRight The top left of the screen
     * @param bottomLeft The top right of the screen
     */
    private compute_far_chunks_on_screen(topRight: LatLng, bottomLeft: LatLng) {

    }

    /**
     * Converts the raw distance values to rounded values (either in km or m)
     * @param width 
     * @param height 
     */
    private compute_dimensions_units(width: number, height: number) {
        if(width >= 1000) {
        this.DEBUG_currentScreenWidth = Math.round(width/1000*100)/100;
        this.DEBUG_currentScreenWidthUnits = 'km';
        } else {
        this.DEBUG_currentScreenWidth = Math.round(width*100)/100;
        this.DEBUG_currentScreenWidthUnits = 'm';
        }

        if(height >= 1000) {
        this.DEBUG_currentScreenHeight = Math.round(height/1000*100)/100;
        this.DEBUG_currentScreenHeightUnits = 'km';
        } else {
        this.DEBUG_currentScreenHeight = Math.round(height*100)/100;
        this.DEBUG_currentScreenHeightUnits = 'm';
        }
    }

    /**
     * Initialises the various button configurations.
     *
     * First generates a base button configuration that every other button clones
     * via a deep copy, before altering each button to give it a unique icon.
     */
    private init_button_configs() {
        const baseButtonConfig: CustomButtonConfig = {
        buttonType: CustomButtonType.REGULAR_BUTTON,
        height: 36,
        scale: undefined,
        width: 130,
        toggle: false,
        toggleClass: 'button-flat-colour-on',
        icon: "",
        regularButtonIconEnable: false,
        isDisabled: false
        };

        this.DEBUG_toggleChunkLoadingButtonConfig = this.generalHelper
                .clone_object(baseButtonConfig);
    }


    /**
     * Computes the screen width and height for the debug menu
     * @param topRight 
     * @param bottomLeft 
     * @returns 
     */
    private compute_screen_metrics(topRight: LatLng, bottomLeft: LatLng) {
        const screenWidth = this.calculate_distance(topRight.latitude, 
            topRight.longitude, topRight.latitude, bottomLeft.longitude);
        const screenHeight = this.calculate_distance(topRight.latitude,
            topRight.longitude, bottomLeft.latitude, topRight.longitude);

        return [screenWidth, screenHeight];
    }

    /**
     * Given a search radius, this method computes the set of latitude and
     * longitude points required to cover a specified search area.
     * @param lat1 The latitude of the first coordinate
     * @param lon2 The longitude of the first coordinate
     * @param lat2 The latitude of the second coordinate
     * @param lon2 The longitude of the second coordinate
     * 
     */
    private calculate_distance(lat1: number, lon1: number, lat2: number, 
            lon2: number) {
        const R = 6371e3; // metres
        const φ1 = lat1 * Math.PI/180; // φ, λ in radians
        const φ2 = lat2 * Math.PI/180;
        const Δφ = (lat2-lat1) * Math.PI/180;
        const Δλ = (lon2-lon1) * Math.PI/180;

        const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ/2) * Math.sin(Δλ/2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        const d = R * c; // in metres

        return d;
    }
    
    /**
     * 
     * 
     */
    private road_detail_check() {
        if(this.currentZoomLevel <= 7) {
            this.DEBUG_roadDetail = 'FAR';
            return ChunkType.FAR;
        } else if(this.currentZoomLevel >= 8 && this.currentZoomLevel <= 10) {
            this.DEBUG_roadDetail = 'STANDARD';
            return ChunkType.STANDARD;
        } else if(this.currentZoomLevel >= 11 && this.currentZoomLevel <= 13) {
            this.DEBUG_roadDetail = 'NEAR';
            return ChunkType.NEAR;
        } else if (this.currentZoomLevel >= 14) {
            this.DEBUG_roadDetail = 'CLOSE';
            return ChunkType.CLOSE;
        }

        // if(widthKM >= 0 && widthKM <= 5) {
        //     this.DEBUG_roadDetail = 'CLOSE';
        // } else if(widthKM > 5 && widthKM <= 10 ) {
        //     this.DEBUG_roadDetail = 'NEAR';
        // } else if(widthKM > 10 && widthKM <= 100) {
        //     this.DEBUG_roadDetail = 'STANDARD';
        // } else {
        //     this.DEBUG_roadDetail = 'FAR';
        // }
    }

    // * PRIVATE VARIABLES -----------------------------------------------------
    private currentBounds: any;
    private chunkLoaderEnabled: boolean = true;

    private previouslyClickedArm: Arms = undefined;
    private previouslyClickedIntersec: Intersection = undefined;

    private standardColour: string = '#00AAFF';
    private forecastAvailableColour: string = '#ba68c8';
    private roadHighlightColour: string = '#f44336';

    // ? CHUNK GETTER STREAM API -----------------------------------------------

    public request_grab_chunks_stream(): void {
        this.testTriangle = [];
        this.intersections = [];

        // Disable the refresh button to prevent spamming
        this.refreshButtonRef.disable_button();

        // Show the loading spinner
        this.showRefreshSpinner = true;
        
        this.chunkGetterApiHelper.grab_chunks_stream(this, 
                this.grabChunksStreamInputData);
    }

    public grab_chunks_on_data_callback(data: string): void {
        console.log("Received data:");
        const dataAsDict = JSON.parse(data['array'][1]); 
        const status = data['array'][0];
        const errMsg = data['array'][2];
        const forecast = data['array'][3];

        let forecastAvailable: boolean;
        let baseColour: string;
        if(forecast == 1) {
            console.log("FORECAST AVAILABLE! " +forecast);
            forecastAvailable = true;
            baseColour = this.forecastAvailableColour;
        } else {
            forecastAvailable = false;
            baseColour = this.standardColour;
        }

        console.log(dataAsDict);
        // this.testTriangle.push([
        //         { lat: dataAsDict['vert_1']['lat'], 
        //             lng: dataAsDict['vert_1']['lng'] },
        //         { lat: dataAsDict['vert_2']['lat'], 
        //             lng: dataAsDict['vert_2']['lng'] },
        //         { lat: dataAsDict['vert_3']['lat'], 
        //             lng: dataAsDict['vert_3']['lng'] },
        // ]);
        let listOfArms: Arms[] = []
        for(let i = 0; i < dataAsDict['arms'].length; i++) {
            let listOfSteps: Step[] = []
            const currentArm = dataAsDict['arms'][i];
            for(let j = 0; j < currentArm['route']['steps'].length; j++) {
                let listOfCoords: LatLngLiteral[] = []
                const currentStep = currentArm['route']['steps'][j];
                for(let k = 0; k < currentStep['coords'].length; k++) {
                    const currentCoord = currentStep['coords'][k];
                    listOfCoords.push({
                        lat: currentCoord['lat'],
                        lng: currentCoord['lng']
                    });
                }
                listOfSteps.push({
                    coords: listOfCoords,
                    distanceValue: currentStep['distance_value'],
                    durationValue: currentStep['duration_value']
                });
            }
            listOfArms.push({
                nextTsc: currentArm['next_tsc'],
                route: {
                    steps: listOfSteps
                },
                streetName: currentArm['street_name'],
                colour: baseColour,
                originTsc: dataAsDict['tsc'],
                forecastAvailable: forecastAvailable
            });
        }
        this.intersections.push({
            arms: listOfArms,
            origin: {
                lat: dataAsDict['origin']['lat'],
                lng: dataAsDict['origin']['lng']
            },
            tsc: dataAsDict['tsc'],
            colour: baseColour,
            forecastAvailable: forecastAvailable
        });
    }

    public grab_chunks_on_status_callback(status: any): void {
        console.log("Received status:");
        console.log(status);
    }

    public grab_chunks_on_error_callback(errMsg: string): void {
        console.log("Received error:");
        if(!this.errorDebouncer && 
                this.loginHelper.check_jwt_expired_error(errMsg)) {
            // Handle debouncing
            this.errorDebouncer = true;
            this.timeoutId = setTimeout(() => {
                this.errorDebouncer = false;
            }, 500);

            this.grabChunksStreamNumRetries++;
            // Keep track of how many retries this is consuming.
            if(this.grabChunksStreamNumRetries > 5) {
                return;
            }

            console.log('refreshing jwt');

            // Go ahead with the retry.
            this.loginApiHelper.send_refresh_jwt_request(this, 
                    this.grabChunksStreamApiMasterId);
        } else {
            console.error(errMsg);
        }
    }

    public grab_chunks_on_end_callback(): void {
        console.log("Stream has ended");
        this.refreshButtonRef.enable_button();
        this.showRefreshSpinner = false;
    }

    public grabChunksStreamApiMasterId: ApiIdMaster = ApiIdMaster.
            GRAB_CHUNKS_STREAM;
    public grabChunksStreamNumRetries: number = 0;
    public grabChunksStreamInputData: Viewport;

    private errorDebouncer: boolean = false;
    private timeoutId;
}
