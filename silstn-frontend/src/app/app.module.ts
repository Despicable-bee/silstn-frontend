import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// * Material Imports ----------------------------------------------------------

import {
  AngularMaterialModule
} from './material-module';

// * Forms Imports -------------------------------------------------------------

import { 
  FormsModule, 
  ReactiveFormsModule 
} from '@angular/forms';

// * Third party imports -------------------------------------------------------

import {
  CookieModule
} from 'ngx-cookie';

import { 
  AgmOverlays 
} from "agm-overlays"

import {
  AgmCoreModule
} from '@agm/core';

import { 
  NgxChartsModule 
} from '@swimlane/ngx-charts';

// * Our Modules ---------------------------------------------------------------

import { 
  AppRoutingModule 
} from './app-routing.module';

import { 
  AppComponent 
} from './app.component';

import { 
  BrowserAnimationsModule 
} from '@angular/platform-browser/animations';

// * PAGES ---------------------------------------------------------------------

import { 
  LoginComponent 
} from './pages/login/login.component';

import { 
  HomeComponent 
} from './pages/home/home.component';

// * SUBCOMPONENTS -------------------------------------------------------------

import { 
  CustomButtonComponent 
} from './subcomponents/custom-button/custom-button.component';

import { 
  TextInputFieldComponent 
} from './subcomponents/text-input-field/text-input-field.component';

import { 
  LoadingSpinnerComponent 
} from './subcomponents/loading-spinner/loading-spinner.component';

import { 
  MapComponent 
} from './subcomponents/map/map.component';
import { DataDisplayerComponent } from './subcomponents/data-displayer/data-displayer.component';
import { DataDisplayerSubmenuComponent } from './subcomponents/data_displayer_submenu/data-displayer-submenu/data-displayer-submenu.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomButtonComponent,
    LoginComponent,
    HomeComponent,
    TextInputFieldComponent,
    LoadingSpinnerComponent,
    MapComponent,
    DataDisplayerComponent,
    DataDisplayerSubmenuComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    CookieModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBkGJaqO5x2uOTT3mknrMozrx0UeLJ6G90'
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
