// ANGULAR ---------------------------------------------------------------------

import { 
  Component,
  HostListener,
  PLATFORM_ID,
  Inject,
  ViewChild,
  Host,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';

import {
  isPlatformBrowser,
  isPlatformServer
} from '@angular/common';


// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
  GeneralMethodsService
} from 'src/app/services/helper_methods/shared/general_methods/general-methods.service';

import {
  ResizeHelperService
} from 'src/app/services/helper_methods/shared/resize_helper/resize-helper.service';

import {
  ThemeTrackerService
} from 'src/app/services/helper_methods/shared/theme_tracker/theme-tracker.service';

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

import {
  PageRoutingService
} from 'src/app/services/helper_methods/shared/page_routing_service/page-routing.service';  

// INTERFACES ------------------------------------------------------------------

import {
  WindowResize
} from 'src/app/services/helper_methods/shared/resize_helper/resize-helper.interface';

// APIS ------------------------------------------------------------------------

// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

import {
  CustomButtonType
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';

// DATA STRUCTURES -------------------------------------------------------------

import {
  CustomButtonConfig
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';

import { 
  WindowStates 
} from 'src/app/data_structures/shared/window-states/window-states.ds';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements WindowResize {

  constructor(private generalHelper: GeneralMethodsService,
      private windowResizeHelper: ResizeHelperService,
      private themeTrackerHelper: ThemeTrackerService,
      private loginHelper: LoginHelperService,
      private router: PageRoutingService,
      @Inject(PLATFORM_ID) platformId) {
    this.isBrowser = isPlatformBrowser(platformId);
    
    // Only render this stuff if this is a browser.
    if(this.isBrowser) {
      // Init toolbar buttons
      this.toolbar_buttons_init();
      this.windowResizeHelper.get_screen_width_observable().subscribe(
        result => {
          this.window_state_change(result)
        });
      
      // Get the initial window state
      this.window_state_change(
          this.windowResizeHelper.get_current_screen_state());

      // Init theme
      if(this.themeTrackerHelper.get_theme() == 'dark') {
        this.isDarkTheme = true;
      }
    }
  }

  title = 'silstn-frontend';

  // * PUBLIC METHODS ----------------------------------------------------------

  public toolbar_button_handler(type: string) {
    switch(type) {
      case 'logo':
        break;
      case 'settings':
        break;
      case 'logout':
        this.loginHelper.logout();
        this.router.redirect('login', undefined);
        break;
      case 'toggle-dark-mode':
        this.toggle_dark_mode();
        break;
      default:
        console.error("Unknown toolbar button type")
    }
  }

  public window_state_change(newState: WindowStates) {
    switch(newState) {
      case WindowStates.XSMALL:
        // Resize elements to be XSMALL (x < 600)
        // Update height of background filler
        this.toolbarBackgroundFillerHeight = '56px';
        this.mainBackgroundHeight = 'calc(100% - 56px)';
        break;
      case WindowStates.SMALL:
        // Resize elements to be SMALL (600 <= x <1024)
        this.toolbarBackgroundFillerHeight = '64px';
        this.mainBackgroundHeight = 'calc(100% - 64px)';
        break;
      case WindowStates.MEDIUM:
        // Resize elements to be MEDIUM (1024 <= x < 1440)
        this.toolbarBackgroundFillerHeight = '64px';
        this.mainBackgroundHeight = 'calc(100% - 64px)';
        break;
      case WindowStates.LARGE:
        // Resize elements to be LARGE (1440 <= x < 1920)
        this.toolbarBackgroundFillerHeight = '64px';
        this.mainBackgroundHeight = 'calc(100% - 64px)';
        break;
      case WindowStates.XLARGE:
        // Resize elements to be XLARGE (1920 <= x)
        this.toolbarBackgroundFillerHeight = '64px';
        this.mainBackgroundHeight = 'calc(100% - 64px)';
        break;
      case WindowStates.SERVER:
        this.toolbarBackgroundFillerHeight = '64px';
        this.mainBackgroundHeight = 'calc(100% - 64px)';
        break;
      default:
        console.error("Unknown windows state: " + newState);
    }
  }


  // * PUBLIC VARIABLES --------------------------------------------------------

  public toolbarBackgroundFillerHeight: string = "64px";
  public mainBackgroundHeight: string = "calc(100% - 64px)"

  public showToolbar: boolean = true;

  public isDarkTheme: boolean = false;

  public matTooltipDelay: number = 250;

  public settingsButtonText: string = "Settings";
  public settingsButtonConfig: CustomButtonConfig;

  public isBrowser: boolean = false;


  // * PRIVATE METHODS ---------------------------------------------------------

  private toolbar_buttons_init() {
    const commonToolbarConfigTemplate: CustomButtonConfig = {
      buttonType: CustomButtonType.TOOLBAR_BUTTON,
      height: 40,
      icon: '',
      regularButtonIconEnable: true,
      scale: 1,
      toggle: false,
      toggleClass: 'toolbar-button-colour-on',
      width: 40,
      isDisabled: false
    }

    this.settingsButtonConfig = this.generalHelper.clone_object(
      commonToolbarConfigTemplate);
    this.settingsButtonConfig.icon = 'more_vert';
  }

  /** 
   * Toggles the dark theme for the site
   */
   private toggle_dark_mode() {
    if(this.isDarkTheme) {
      this.isDarkTheme = false;
      this.themeTrackerHelper.set_theme('light');
    } else {
      this.isDarkTheme = true;
      this.themeTrackerHelper.set_theme('dark');
    }
  }


  // * PRIVATE VARIABLES -------------------------------------------------------
}
