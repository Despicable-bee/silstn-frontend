import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  AuthGuardService
} from 'src/app/services/auth_guard/auth-guard.service';

// COMPONENTS ------------------------------------------------------------------

import {
  LoginComponent
} from 'src/app/pages/login/login.component';

import {
  HomeComponent
} from 'src/app/pages/home/home.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy', initialNavigation: 'enabledBlocking' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
