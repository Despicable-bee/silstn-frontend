// ANGULAR ---------------------------------------------------------------------

import { 
  Component, 
  OnInit 
} from '@angular/core';

import { 
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
  TextInputFieldService
} from 'src/app/services/helper_methods/specific/text_input_fields_service/text-input-field.service';

import {
  GeneralMethodsService
} from 'src/app/services/helper_methods/shared/general_methods/general-methods.service';

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

import {
  PageRoutingService
} from 'src/app/services/helper_methods/shared/page_routing_service/page-routing.service';


// INTERFACES ------------------------------------------------------------------

import {
  UserCreateAccount,
  UserLogin
} from 'src/app/services/api_calls/specific/login_api/login-api.interface';


// APIS ------------------------------------------------------------------------

import {
  LoginApiService
} from 'src/app/services/api_calls/specific/login_api/login-api.service'


// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

// DATA STRUCTURES -------------------------------------------------------------

import { 
  FormConfig, 
  FormType 
} from 'src/app/data_structures/specific/text-input-field/text-input-field.ds';

import { 
  CustomButtonConfig, 
  CustomButtonType 
} from 'src/app/data_structures/specific/custom-button/custom-button.ds';

import {
  UserLoginRequestDatastructure,
  UserLoginRequestType,
  UserCreateAccountRequestDataStructure,
  UserCreateAccountStatus
} from 'src/app/data_structures/specific/login/login.ds';
import { ProgressSpinnerConfig } from 'src/app/data_structures/shared/loading-spinner/loading-spinner.ds';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, UserLogin, UserCreateAccount {

  constructor(private formBuilder: FormBuilder,
      private textFieldHelper: TextInputFieldService,
      private generalHelper: GeneralMethodsService,
      private loginAPIHelper: LoginApiService,
      private loginHelper: LoginHelperService,
      private pageNavHelper: PageRoutingService) { 
    this.initialise_form_configs();
    this.initialise_button_configs();
    this.init_loading_spinner();
  }

  ngOnInit(): void {
    // Check if we have jwts
    if(this.loginHelper.check_cookies_exist()) {
      // Attempt to login with cookies
      console.log("Logging in with JWT");
      this.get_user_login_inputs(UserLoginRequestType.LOGIN_W_JWT);
      this.request_user_login();
    }
  }

  // * PUBLIC METHODS ----------------------------------------------------------

  /**
   * Handles input from the various login fields.
   * @param type 
   */
  public login_input_handler(type: string) {

  }

  /**
   * Handles input from the various create account fields.
   * @param type 
   */
  public create_account_input_handler(type: string) {

  }

  public button_handler(type: string){
    switch(type) {
      case 'login':
        // Gather all the login data
        this.get_user_login_inputs(UserLoginRequestType.LOGIN_W_FORM);
        this.request_user_login();
        break;
      case 'show-create':
        this.showErrorMessage = false;
        this.createAccountFormGroup.reset();
        this.showLogin = false;
        this.showCreateAccount = true;
        break;
      case 'show-login':
        this.showErrorMessage = false;
        this.loginFormGroup.reset();
        this.showLogin = true;
        this.showCreateAccount = false;
        break;
      case 'submit':
        // Gather all the create account data
        this.get_user_create_account_inputs();
        this.request_user_create_account();
        break;
      default:
        console.error("Unknown button type: " + type);
    }
  }

  // * PUBLIC VARIABLES --------------------------------------------------------

  // Login input configs
  public loginEmailConfig: FormConfig;
  public loginPasswordConfig: FormConfig;
  
  // Create account input configs
  public createFirstnameConfig: FormConfig;
  public createSurnameConfig: FormConfig;
  public createEmailConfig: FormConfig;
  public createDobConfig: FormConfig;
  public createPasswordConfig: FormConfig;
  public createConfirmPasswordConfig: FormConfig;

  // Login buttons
  public loginButtonConfig: CustomButtonConfig;
  public showCreateAccountButtonConfig: CustomButtonConfig;  

  // Create Account buttons
  public cancelCreateAccountButtonConfig: CustomButtonConfig;
  public submitCreateAccountButtonConfig: CustomButtonConfig;

  // Flags for showing/hiding content
  public showLogin: boolean = true;
  public showCreateAccount: boolean = false;
  public showErrorMessage: boolean = false;

  // Error message data
  public loginErrorMessage: string;
  public showLoginErrorMessage: boolean = false;
  public createAccountErrorMessage: string;
  public showCreateAccountErrorMessage: boolean = false;
  public showCreateAccountSpinner: boolean = false;
  public showLoginSpinner: boolean = false;

  public loadingSpinnerConfig: ProgressSpinnerConfig;


  // * PRIVATE METHODS ---------------------------------------------------------

  private init_loading_spinner() {
    this.loadingSpinnerConfig = {
      colour: 'accent',
      diameter: 40,
      mode: 'indeterminate',
      value: 0
    } 
  }

  /**
   * Formats the user login data structure with info depending on the users
   * intended login method.
   * @param type Either LOGIN_W_FORM or LOGIN_W_JWT
   */
   private get_user_login_inputs(type: UserLoginRequestType) {
    if(type == UserLoginRequestType.LOGIN_W_FORM) {
      this.userLoginDataStruct = {
        email: this.loginFormGroup.get('email').value,
        loginType: type,
        password: this.loginFormGroup.get('password').value,
        jwtOld: undefined,
        refreshOld: undefined
      }
    } else {
      this.userLoginDataStruct = {
        email: undefined,
        loginType: type,
        password: undefined,
        jwtOld: this.loginHelper.get_jwt(),
        refreshOld: this.loginHelper.get_refresh()
      }
    }
  }

  private get_user_create_account_inputs() {
    const date = this.createAccountFormGroup.get('dob').value;
    const formattedData = date.getDate() + '/' + (date.getMonth() + 1) + '/'
        + date.getFullYear();
    this.userCreateAccountDataStruct = {
      firstname: this.createAccountFormGroup.get('firstname').value,
      surname: this.createAccountFormGroup.get('surname').value,
      email: this.createAccountFormGroup.get('email').value,
      dob: formattedData,
      password: this.createAccountFormGroup.get('password').value
    }
  }


  private initialise_button_configs() {
    const baseButtonConfig: CustomButtonConfig = {
      buttonType: CustomButtonType.REGULAR_BUTTON,
      height: 36,
      scale: undefined,
      width: 140,
      toggle: false,
      toggleClass: 'button-flat-colour-on',
      icon: "",
      regularButtonIconEnable: true,
      isDisabled: false
    };

    this.loginButtonConfig = this.generalHelper.clone_object(baseButtonConfig);
    this.loginButtonConfig.icon = 'login';
    this.loginButtonConfig.width = 300;

    this.showCreateAccountButtonConfig = this.generalHelper.clone_object(
        baseButtonConfig);
    this.showCreateAccountButtonConfig.icon = 'person';
    this.showCreateAccountButtonConfig.width = 300;

    this.cancelCreateAccountButtonConfig = this.generalHelper.clone_object(
      baseButtonConfig);
    this.cancelCreateAccountButtonConfig.icon = 'close';

    this.submitCreateAccountButtonConfig = this.generalHelper.clone_object(
      baseButtonConfig);
    this.submitCreateAccountButtonConfig.icon = 'send';

  }

  /**
   * Initialises the various form configurations for both login and create
   *  account FormConfig variables.
   */
  private initialise_form_configs() {
    const formWidth = 300;
    // init the login form group
    this.loginFormGroup = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });

    this.loginEmailConfig = this.textFieldHelper.create_form_config(
      this.loginFormGroup,
      'Email', 'email', 80, formWidth, FormType.INPUT_FORM,
      'someone@somewhere.com', 'text',
      false, 1, false
    );

    this.loginPasswordConfig = this.textFieldHelper.create_form_config(
      this.loginFormGroup,
      'Password', 'password', 80, formWidth, FormType.INPUT_FORM,
      '12345', 'password',
      false, 1, false
    );

    // Init the create account form group
    this.createAccountFormGroup = this.formBuilder.group({
      firstname: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      dob: [{value: '', disabled: true}, Validators.required],
      password: ['', Validators.required],
      cpassword: ['', Validators.required]
    });

    this.createFirstnameConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Firstname', 'firstname', 80, formWidth, FormType.INPUT_FORM,
      'John', 'text',
      false, 1, false
    );

    this.createSurnameConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Surname', 'surname', 80, formWidth, FormType.INPUT_FORM,
      'Smith', 'text',
      false, 1, false
    );

    this.createEmailConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Email', 'email', 80, formWidth, FormType.INPUT_FORM,
      'someone@somewhere.com', 'text',
      false, 1, false
    );

    this.createDobConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Date of Birth', 'dob', 80, formWidth, FormType.DATE_PICKER_FORM,
      '01/01/1970', 'text',
      true, 1, false
    );

    this.createPasswordConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Password', 'password', 80, formWidth, FormType.INPUT_FORM,
      '12345', 'password',
      false, 1, false
    );

    this.createConfirmPasswordConfig = this.textFieldHelper.create_form_config(
      this.createAccountFormGroup,
      'Confirm password', 'cpassword', 80, formWidth, FormType.INPUT_FORM,
      '12345', 'password',
      false, 1, false
    );
  }

  private create_account_error_message_formatter(errMsg: string) {
    const formattedString = errMsg.replace(' ', '').split(':')
    switch(formattedString[0]) {
      case 'UserProfilePasswordAssert':
        if(formattedString[1] == 'INVALID_PASSWORD_FORMAT') {
          this.createAccountErrorMessage = 
          'Invalid password, password must contain a minimum of 8 ' + 
          'chars, at least one uppercase char, one lowercase char, ' +
          'one number and one special char.';
          this.showCreateAccountErrorMessage = true;
        }
        break;
      case 'UserProfilePhonenumberAssert':
        if(formattedString[1] == 'PHONE_NUMBER_FORMAT_INVALID') {
          this.createAccountErrorMessage = "Phone number format invalid";
          this.showCreateAccountErrorMessage = true;
        }  
        break;
      default:
        console.log(errMsg);
    }
  }


  // * PRIVATE VARIABLES -------------------------------------------------------
  private loginFormGroup: FormGroup;
  private createAccountFormGroup: FormGroup;

  // Login data structure persistent variables
  private userLoginDataStruct: UserLoginRequestDatastructure; 
  private userCreateAccountDataStruct: UserCreateAccountRequestDataStructure;

  // ? USER LOGIN API ----------------------------------------------------------

  public request_user_login() {
    console.log("Sending login request");
    this.showLoginErrorMessage = false;
    // Disable the user from changing the form group until 
    this.loginFormGroup.disable();
    this.showLoginSpinner = true;
    this.showCreateAccountSpinner = false;
    this.loginAPIHelper.send_user_login_request(this, this.userLoginDataStruct);
  }

  public user_login_success_callback() { 
    console.log("Login successful");
    this.loginFormGroup.enable();
    this.showLoginSpinner = false;
    // Redirect to the homepage
    this.pageNavHelper.redirect('home', undefined);
  }

  public user_login_unsuccessful_callback(errMsg: string) {
    console.log(errMsg);
    this.loginErrorMessage = errMsg;
    this.showLoginErrorMessage = true;
    this.loginFormGroup.enable();
    this.showLoginSpinner = false;
  }

  public user_login_error_callback(err: any) {
    console.log("Error:");
    console.log(err);
    this.loginErrorMessage = err;
    this.showLoginErrorMessage = true;
    this.loginFormGroup.enable();
    this.showLoginSpinner = false;
  }

  // ? USER CREATE ACCOUNT API -------------------------------------------------

  public request_user_create_account() {
    this.showCreateAccountErrorMessage = false;
    this.showLoginSpinner = false;
    this.showCreateAccountSpinner = true;
    this.loginAPIHelper.send_user_create_account_request(this, 
        this.userCreateAccountDataStruct);
  }

  public user_create_account_success_callback() {
    // Redirect to the homepage
    this.showCreateAccountSpinner = false;
    this.pageNavHelper.redirect('home', undefined);
  }

  public user_create_account_unsuccessful_callback(errMsg: string) {
    this.showCreateAccountSpinner = false;
    this.create_account_error_message_formatter(errMsg);
  }

  public user_create_account_error_callback(err: any) {
    this.showCreateAccountSpinner = false;
    console.log("Create account error");
    console.log(err);
  }

}
