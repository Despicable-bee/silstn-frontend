// ANGULAR ---------------------------------------------------------------------

import { 
  Component, 
  OnInit,
  ViewChild,
} from '@angular/core';


// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

// INTERFACES ------------------------------------------------------------------

// APIS ------------------------------------------------------------------------

// COMPONENTS ------------------------------------------------------------------

import {
  DataDisplayerComponent
} from 'src/app/subcomponents/data-displayer/data-displayer.component';

import {
  MapComponent
} from 'src/app/subcomponents/map/map.component';

// ENUMS -----------------------------------------------------------------------

// DATA STRUCTURES -------------------------------------------------------------

import { 
  Arms 
} from 'src/app/data_structures/shared/agm-map/agm_map.ds';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('dataDisplayerRef') dataDisplayerRef: DataDisplayerComponent;
  @ViewChild('mapRef') mapRef: MapComponent;

  constructor() { }

  ngOnInit(): void {
  }

  // * PUBLIC METHODS ----------------------------------------------------------

  public close_data_displayer_handler() {
    // Hide the data displayer
    this.showDataDisplayer = false;
    
    // Tell the map to clear the active route
    this.mapRef.polyline_clear_click();

  }

  public async route_click_handler($event: Arms) {
    console.log("Received Arm:");
    console.log($event);
    this.showDataDisplayer = true;
    while(this.dataDisplayerRef == undefined) {
      // Wait for 200ms
      await this.delay(200);
    }
    this.dataDisplayerRef.set_current_arm($event);
  }

  // * PUBLIC VARIABLES --------------------------------------------------------

  public showDataDisplayer: boolean = false;

  // * PRIVATE METHODS ---------------------------------------------------------

  private delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  // * PRIVATE VARIABLES -------------------------------------------------------

}
