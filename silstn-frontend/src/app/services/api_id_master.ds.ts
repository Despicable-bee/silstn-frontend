export enum ApiIdMaster {
    // Chunks Getter API
    GRAB_CHUNKS,
    GRAB_CHUNKS_STREAM,
    // Intersection data getter API
    GET_INTERSECTION_DATA,
    CHECK_AVAILABLE_RANGES,
    GET_FORECAST
}