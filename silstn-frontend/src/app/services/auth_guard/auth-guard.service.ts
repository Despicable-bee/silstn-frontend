// ANGULAR ---------------------------------------------------------------------

import { 
  Injectable,
  Inject,
  PLATFORM_ID 
} from '@angular/core';

import {
  isPlatformBrowser,
  isPlatformServer
} from '@angular/common';

import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';

import { 
  Observable 
} from 'rxjs';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

import {
  PageRoutingService
} from 'src/app/services/helper_methods/shared/page_routing_service/page-routing.service';

// INTERFACES ------------------------------------------------------------------

// APIS ------------------------------------------------------------------------

// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

// DATA STRUCTURES -------------------------------------------------------------


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private loginHelper: LoginHelperService,
      private router: Router,
      @Inject(PLATFORM_ID) platformId) { 
    this.isBrowser = isPlatformBrowser(platformId);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
      boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | 
          UrlTree> {
    if(this.isBrowser) {
      if(!this.loginHelper.check_cookies_exist()) {
        return this.router.createUrlTree(['login']);
      } else {
        return true;
      }
    } else {
      return true;
    }
    
  }

  private isBrowser: boolean = false;
}
