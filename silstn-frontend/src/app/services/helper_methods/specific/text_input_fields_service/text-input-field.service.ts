import { Injectable } from '@angular/core';

import {
  FormGroup
} from '@angular/forms';

import {
  FormConfig,
  FormType
} from 'src/app/data_structures/specific/text-input-field/text-input-field.ds';

@Injectable({
  providedIn: 'root'
})
export class TextInputFieldService {

  constructor() { }

  /**
   * Generates a new form config given various inputs.
   * @param parentForm 
   * @param formLabel 
   * @param formControlName 
   * @param maxChars 
   * @param formWidth 
   * @param type 
   * @param placeholder 
   * @param inputType 
   * @returns 
   */
  public create_form_config(parentForm: FormGroup,
      formLabel: string, formControlName: string, maxChars: number,
      formWidth: number, type: FormType, placeholder: string, 
      inputType: string, showCount: boolean, maxRows?: number,
      emitUpdate?: boolean) {

    const formWidthString: string = formWidth.toString() + 'px';

    const newFormConfig: FormConfig = {
      parentForm: parentForm,
      formLabel: formLabel,
      formControlName: formControlName,
      maxRows: maxRows,
      maxCharacters: maxChars,
      placeholderDescription: placeholder,
      formWidth: formWidthString,
      type: type,
      inputType: inputType,
      showCount: showCount,
      emitUpdate: emitUpdate
    }

    return newFormConfig;
    }
}

