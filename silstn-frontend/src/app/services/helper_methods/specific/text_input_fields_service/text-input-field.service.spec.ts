import { TestBed } from '@angular/core/testing';

import { TextInputFieldService } from './text-input-field.service';

describe('TextInputFieldService', () => {
  let service: TextInputFieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TextInputFieldService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
