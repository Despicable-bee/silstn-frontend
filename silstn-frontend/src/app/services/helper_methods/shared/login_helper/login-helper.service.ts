import { Injectable } from '@angular/core';

import {
  CookieService
} from 'ngx-cookie';

import { Inject } from '@angular/core';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class LoginHelperService {

  constructor(private cookieHelper: CookieService,
      @Inject(PLATFORM_ID) platformId) {
    this.isBrowser = isPlatformBrowser(platformId);
    if(this.isBrowser) {
      this.reload_cookies();
    }
    
  }

  /**
   * Sets the JWT and/or refresh token for the user.
   * @param jwt The JWT as a base64 encoded string
   * @param refresh The refresh token as a base64 encoded string
   */
  public set_jwt_refresh(jwt: string | undefined, 
      refresh: string | undefined) {
    
    if(typeof jwt == "string") {
      this.cookieHelper.put("jwt", jwt);
    }

    if(typeof refresh == "string") {
      this.cookieHelper.put("refresh", refresh);
    }

    this.reload_cookies();
  }

  /**
   * @returns The jwt, base64 encoded string, stored in memory.
   */
  public get_jwt() {
    return this.jwtCookie;
  }

  /**
   * @returns The refresh, base64 encoded string, stored in memory. 
   */
  public get_refresh() {
    return this.refreshCookie;
  }

  /**
   * @returns True if cookies already exist, otherwise returns false. 
   */
  public check_cookies_exist() {
    // Reload the cookies that are stored in the browsers local storage
    this.reload_cookies();

    // Check if they exist or not
    if(this.jwtCookie != undefined && this.refreshCookie != undefined) {
      console.log("Cookies exist");
      return true
    } else {
      console.log("Cookies don't exist");
      return false
    }
  }

  /**
   * Determines if an error message recieved by the client is due to an expired
   * JWT
   * @param err The error message to check
   * @returns True if the JWT is expired, otherwise returns false
   */
   public check_jwt_expired_error(err: any) {
    if(err['code'] == 2 && err['message'] == "Unknown Content-type received.") {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Gets the User Id from the jwt.
   * @returns string containing the user id.
   */
  public get_user_id_from_jwt() {
    const decoded = jwt_decode(this.get_jwt());
    return decoded['sub'];
  }

  /**
   * logout the user by removing the cookies.
   * This only handles the clearing. Redirect happens in Implementation.
   */
  public logout() {
    this.cookieHelper.remove("jwt");
    this.cookieHelper.remove("refresh");
  }

  // * PUBLIC VARIABLES --------------------------------------------------------
  

  // * PRIVATE METHODS ---------------------------------------------------------

  /**
   * Takes the data out of cookies and puts them into current memory.
   */
  private reload_cookies() {
    this.jwtCookie = this.cookieHelper.get("jwt");
    this.refreshCookie = this.cookieHelper.get("refresh");
  }

  // * PRIVATE VARIABLES -------------------------------------------------------
  private jwtCookie: string;
  private refreshCookie: string;
  private isBrowser: boolean = false; 

}

