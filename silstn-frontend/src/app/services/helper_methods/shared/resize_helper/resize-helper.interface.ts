import {
    WindowStates
} from 'src/app/data_structures/shared/window-states/window-states.ds';

export interface WindowResize {
    window_state_change(newState: WindowStates): void;
}
