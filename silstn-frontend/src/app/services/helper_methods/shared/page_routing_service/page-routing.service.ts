// * Core imports --------------------------------------------------------------
import { 
  Injectable 
} from '@angular/core';

import { 
  NavigationEnd, 
  Router 
} from '@angular/router';

// * Interfaces ----------------------------------------------------------------
import {
  RedirectInterface
} from './redirect-interface';

// * Data Structures -----------------------------------------------------------
import {
  PageId
} from 'src/app/data_structures/specific/page-routing/page-routing.ds';

@Injectable({
  providedIn: 'root'
})
export class PageRoutingService {

  constructor(private router: Router) {}

  // * PUBLIC METHODS ----------------------------------------------------------
  /**
   * Redirection function, used to navigate the user to a specific (approved)
   * page on the site.
   * ! NOTE:
   * ? This does not stop the user from navigating using the search bar.
   * @param destination A string which specifies where the user wants to go.
   */
   public redirect(destination: string, parameters: string | undefined): void {
    switch(destination) {
      case 'home':
      case 'login':
        if(parameters == undefined) {
          this.router.navigate([`${destination}`]);
        } else {
          this.router.navigate(['/' + destination, parameters]);
        }
        
        break;
      default:
        console.error("Unknown Redirect destination: " + destination);
    }
  }

  /**
   * Determines what the route is based on the text that it's fed
   */
   public async get_route() {
    // Wait for the events to finish
    await this.router.events;
    switch(this.router.url.toString()) {
      case '/login':
        return PageId.LOGIN;
      case '/home':
        return PageId.HOME;
      case '/profile':
        return PageId.PROFILE;
      default:
        console.error('Unknown Route: ' + this.router.url.toString());
        return PageId.UNKNOWN;
    }
  }

  /**
   * TODO - Flesh out for all 5 buttons
   * Depending on which main window the user is in, one of the top buttons will
   *  be highlighted.
   * @returns 
   */
  public async get_toolbar_route_string() {
    await this.router.events;
    switch(this.router.url.toString()) {
      case '/user-projects':
        return 'User project';
      case '/squadrons':
        return 'Squadrons';
      default:
        console.error("Unknown button route: " + this.router.url.toString());
    }
  }


  /**
   * Helper method for something...
   * TODO - Fix explanation
   * @param _this 
   */
  public detect_redirect(_this: RedirectInterface) {
    this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        _this.redirect_callback();
      }
    });
  }

  // * PUBLIC VARIABLES --------------------------------------------------------

  // * PRIVATE METHODS ---------------------------------------------------------

  // * PRIVATE VARIABLES -------------------------------------------------------
  
}
