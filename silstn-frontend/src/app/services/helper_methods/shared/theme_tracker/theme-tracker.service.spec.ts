import { TestBed } from '@angular/core/testing';

import { ThemeTrackerService } from './theme-tracker.service';

describe('ThemeTrackerService', () => {
  let service: ThemeTrackerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeTrackerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
