// ANGULAR ---------------------------------------------------------------------

import { 
  Injectable 
} from '@angular/core';

// THIRD PARTY -----------------------------------------------------------------

// HELPERS ---------------------------------------------------------------------

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';


// INTERFACES ------------------------------------------------------------------

import {
  UserLogin,
  UserCreateAccount
} from 'src/app/services/api_calls/specific/login_api/login-api.interface';

import {
    ChunkGetter,
    ChunkGetterStream
} from 'src/app/services/api_calls/specific/chunk_getter/chunk_getter.interface';

import {
    CheckAvailableRanges,
    GetIntersectionData
} from 'src/app/services/api_calls/specific/intersection_data_getter/intersection_data_getter.interface';

// APIS ------------------------------------------------------------------------

import {
  ApiIdMaster
} from 'src/app/services/api_id_master.ds';

// COMPONENTS ------------------------------------------------------------------

// ENUMS -----------------------------------------------------------------------

// DATA STRUCTURES -------------------------------------------------------------

import {
  UserLoginRequestDatastructure,
  UserLoginRequestType,
  UserLoginStatus,
  UserCreateAccountRequestDataStructure,
  UserCreateAccountStatus
} from 'src/app/data_structures/specific/login/login.ds';


// STUB ------------------------------------------------------------------------

const {
  User_Login_Request,
  User_Login_Response,
  User_Create_Account_Request,
  User_Create_Account_Response
} = require('src/app/services/api_calls/specific/login_api/login_pb.js');

const {
  LoginClient
} = require('src/app/services/api_calls/specific/login_api/login_grpc_web_pb.js');


@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

    constructor(private loginHelper: LoginHelperService) { }

    // * PUBLIC METHODS ----------------------------------------------------------

    /**
     * Sends a request to the server for a JWT and (possibly) a refresh token
     * @param _this 
     * @param data 
     */
    public send_user_login_request(_this: UserLogin, 
            data: UserLoginRequestDatastructure) {
        let LoginService = new LoginClient(
            'https://espv2-login-7nbpg3wsnq-ts.a.run.app');
        
        let request = new User_Login_Request();

        // Propagate request with specific data
        if(data.loginType == UserLoginRequestType.LOGIN_W_FORM) {
        request.setEmail(data.email);
        request.setPassword(data.password);
        } else {
        request.setJwtold(data.jwtOld);
        request.setRefreshold(data.refreshOld);
        }
        request.setLogintype(data.loginType);

        let __this = this;

        LoginService.user_login(request, {}, 
                function(err, response: typeof User_Login_Response) {
            // Check if we recieved an error
            if(err != null) {
                _this.user_login_error_callback(err);
            } else {
                const responseArray = response['array'];
                // Check what the status is
                switch(responseArray[0]) {
                case UserLoginStatus.SUCCESSFUL:
                    // Save the JWT and refresh
                    __this.loginHelper.set_jwt_refresh(responseArray[1], 
                        responseArray[2]);
                    // Send a callback to the user
                    _this.user_login_success_callback();
                    break;
                case UserLoginStatus.FAILURE:
                    // The users input was wrong
                    console.log(responseArray);
                    _this.user_login_unsuccessful_callback(responseArray[3]);
                    break;
                case UserLoginStatus.ERROR:
                    // Something went wrong with the server
                    _this.user_login_error_callback(responseArray[3]);
                    break;
                default:
                    console.error("Unknown return Status: " + responseArray[0]);
                }
            }
        });
    }

    /**
     * Sends a request to the server to create a user profile. If successful,
     * the method will update the users cookies with a JWT and refresh token.
     * @param _this A reference to the caller which implements the 
     *    UserCreateAccount interface
     * @param data The data to send to the server 
     */
    public send_user_create_account_request(_this: UserCreateAccount,
            data: UserCreateAccountRequestDataStructure) {
        let LoginService = new LoginClient(
            'https://espv2-login-7nbpg3wsnq-ts.a.run.app');

        let request = new User_Create_Account_Request();

        request.setFirstname(data.firstname);
        request.setSurname(data.surname);
        request.setEmail(data.email);
        request.setDob(data.dob);
        request.setPassword(data.password);

        let __this = this;

        LoginService.user_create_account(request, {}, 
            function(err, response: typeof User_Create_Account_Response) {
            if(err != null) {
                _this.user_create_account_error_callback(err);
            } else {
                const responseArray = response['array'];
                switch(responseArray[0]) {
                case UserCreateAccountStatus.SUCCESSFUL:
                    // Save the JWT and refresh
                    __this.loginHelper.set_jwt_refresh(responseArray[1], 
                        responseArray[2]);
                    // Send callback to the component
                    _this.user_create_account_success_callback();
                    break;
                case UserCreateAccountStatus.INVALID_INPUT:
                    // Users input was invalid
                    _this.user_create_account_unsuccessful_callback(
                            responseArray[3]);
                    break;
                case UserCreateAccountStatus.ERROR:
                    // Something went wrong with the server
                    _this.user_create_account_error_callback(responseArray[3]);
                    break;
                default:
                    console.error("Unknown return Status: " + responseArray[0]);
                }
            }
        });
    }

    /**
     * Sends a request to the server to refresh their JWT. Upon recieving a 
     * response, the method will call the appropriate callback depending on the
     * 'type' variable
     * @param _this The reference to the method that implements a particular
     *  interface
     * @param type The api id, which tells the callback router what interface to
     *  case to, and what callback to call. 
     */
    public send_refresh_jwt_request(_this: any, type: ApiIdMaster) {
        let LoginService = new LoginClient(
        'https://espv2-login-7nbpg3wsnq-ts.a.run.app');

        let request = new User_Login_Request();

        // Propagate request with specific data
        request.setJwtold(this.loginHelper.get_jwt());
        request.setRefreshold(this.loginHelper.get_refresh());
        request.setLogintype(UserLoginRequestType.LOGIN_W_JWT);

        let __this = this;
        console.log("Refreshing JWT")
        LoginService.user_login(request, {}, 
                function(err, response: typeof User_Login_Response) {
            // Check if we recieved an error
            if(err != null) {
                console.log("Refresh error 1");
                __this.jwt_refresh_callback_router(_this, type, false);
            } else {
                const responseArray = response['array'];
                // Check what the status is
                switch(responseArray[0]) {
                case UserLoginStatus.SUCCESSFUL:
                    // Save the JWT and refresh
                    console.log("Refresh successful");
                    __this.loginHelper.set_jwt_refresh(responseArray[1], 
                        responseArray[2]);
                    __this.jwt_refresh_callback_router(_this, type, true);
                    break;
                case UserLoginStatus.FAILURE:
                    // The users input was wrong
                    console.log("Refresh failed");
                    __this.jwt_refresh_callback_router(_this, type, false);
                    break;
                case UserLoginStatus.ERROR:
                    console.log("Refresh error 2");
                    console.log(responseArray[3]);
                    // Something went wrong with the server
                    __this.jwt_refresh_callback_router(_this, type, false);
                    break;
                default:
                    console.log("Refresh error 3");
                    // TODO error handling
                    __this.jwt_refresh_callback_router(_this, type, false);
                }
            }
        });
    }

    // * PUBLIC VARIABLES --------------------------------------------------------

    // * PRIVATE METHODS ---------------------------------------------------------

    private jwt_refresh_callback_router(_this: any, type: ApiIdMaster, 
            success: boolean) {
        switch(type) {
            case ApiIdMaster.GRAB_CHUNKS:
                let grabChunks = _this as ChunkGetter;
                if(success)
                    grabChunks.request_grab_chunks();
                else
                    grabChunks.grab_chunks_unsuccessful_callback(
                            'Error Refreshing JWT');
                break;
            case ApiIdMaster.GRAB_CHUNKS_STREAM:
                let grabchunksStream = _this as ChunkGetterStream;
                if(success)
                    grabchunksStream.request_grab_chunks_stream();
                else
                    grabchunksStream.grab_chunks_on_error_callback(
                            'Error Refreshing JWT');
                break;
            case ApiIdMaster.CHECK_AVAILABLE_RANGES:
                let checkAvailableRanges = _this as CheckAvailableRanges;
                if(success)
                    checkAvailableRanges.request_check_available_ranges()
                else
                    checkAvailableRanges.check_available_ranges_error_callback(
                            "Error Refreshing JWT");
            case ApiIdMaster.GET_INTERSECTION_DATA:
                let getIntersectionData = _this as GetIntersectionData;
                if(success)
                    getIntersectionData.request_get_intersection_data_stream();
                else
                    getIntersectionData.get_intersection_data_on_error_callback(
                            "Error Refreshing JWT");
            default:
                console.error("Unknown callback")
              
        }
    }


    // * PRIVATE VARIABLES -------------------------------------------------------
}
