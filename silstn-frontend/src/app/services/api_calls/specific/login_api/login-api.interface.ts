import {
    UserCreateAccountStatus
} from 'src/app/data_structures/specific/login/login.ds';

export interface UserLogin {
    request_user_login(): void;
    user_login_success_callback(): void;
    user_login_unsuccessful_callback(errMsg: string): void;
    user_login_error_callback(err: any): void;
}

export interface UserCreateAccount {
    request_user_create_account(): void;
    user_create_account_success_callback(): void;
    user_create_account_unsuccessful_callback(errMsg: string): void;
    user_create_account_error_callback(err: any): void;
}
