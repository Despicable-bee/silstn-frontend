/**
 * @fileoverview gRPC-Web generated client stub for endpoints.silstn.login
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.endpoints = {};
proto.endpoints.silstn = {};
proto.endpoints.silstn.login = require('./login_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.login.LoginClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.login.LoginPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.login.User_Login_Request,
 *   !proto.endpoints.silstn.login.User_Login_Response>}
 */
const methodDescriptor_Login_user_login = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.login.Login/user_login',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.login.User_Login_Request,
  proto.endpoints.silstn.login.User_Login_Response,
  /**
   * @param {!proto.endpoints.silstn.login.User_Login_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.login.User_Login_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.login.User_Login_Request,
 *   !proto.endpoints.silstn.login.User_Login_Response>}
 */
const methodInfo_Login_user_login = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.login.User_Login_Response,
  /**
   * @param {!proto.endpoints.silstn.login.User_Login_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.login.User_Login_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.login.User_Login_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.login.User_Login_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.login.User_Login_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.login.LoginClient.prototype.user_login =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.login.Login/user_login',
      request,
      metadata || {},
      methodDescriptor_Login_user_login,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.login.User_Login_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.login.User_Login_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.login.LoginPromiseClient.prototype.user_login =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.login.Login/user_login',
      request,
      metadata || {},
      methodDescriptor_Login_user_login);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.login.User_Create_Account_Request,
 *   !proto.endpoints.silstn.login.User_Create_Account_Response>}
 */
const methodDescriptor_Login_user_create_account = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.login.Login/user_create_account',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.login.User_Create_Account_Request,
  proto.endpoints.silstn.login.User_Create_Account_Response,
  /**
   * @param {!proto.endpoints.silstn.login.User_Create_Account_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.login.User_Create_Account_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.login.User_Create_Account_Request,
 *   !proto.endpoints.silstn.login.User_Create_Account_Response>}
 */
const methodInfo_Login_user_create_account = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.login.User_Create_Account_Response,
  /**
   * @param {!proto.endpoints.silstn.login.User_Create_Account_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.login.User_Create_Account_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.login.User_Create_Account_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.login.User_Create_Account_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.login.User_Create_Account_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.login.LoginClient.prototype.user_create_account =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.login.Login/user_create_account',
      request,
      metadata || {},
      methodDescriptor_Login_user_create_account,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.login.User_Create_Account_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.login.User_Create_Account_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.login.LoginPromiseClient.prototype.user_create_account =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.login.Login/user_create_account',
      request,
      metadata || {},
      methodDescriptor_Login_user_create_account);
};


module.exports = proto.endpoints.silstn.login;

