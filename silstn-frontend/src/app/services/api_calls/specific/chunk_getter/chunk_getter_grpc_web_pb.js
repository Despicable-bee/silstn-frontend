/**
 * @fileoverview gRPC-Web generated client stub for endpoints.silstn.chunk_getter
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.endpoints = {};
proto.endpoints.silstn = {};
proto.endpoints.silstn.chunk_getter = require('./chunk_getter_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.chunk_getter.ChunkGetterClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.chunk_getter.ChunkGetterPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.chunk_getter.ChunkGetter_Request,
 *   !proto.endpoints.silstn.chunk_getter.ChunkGetter_Response>}
 */
const methodDescriptor_ChunkGetter_grab_chunks = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.chunk_getter.ChunkGetter/grab_chunks',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.endpoints.silstn.chunk_getter.ChunkGetter_Request,
  proto.endpoints.silstn.chunk_getter.ChunkGetter_Response,
  /**
   * @param {!proto.endpoints.silstn.chunk_getter.ChunkGetter_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.chunk_getter.ChunkGetter_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.chunk_getter.ChunkGetter_Request,
 *   !proto.endpoints.silstn.chunk_getter.ChunkGetter_Response>}
 */
const methodInfo_ChunkGetter_grab_chunks = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.chunk_getter.ChunkGetter_Response,
  /**
   * @param {!proto.endpoints.silstn.chunk_getter.ChunkGetter_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.chunk_getter.ChunkGetter_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.chunk_getter.ChunkGetter_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.chunk_getter.ChunkGetter_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.chunk_getter.ChunkGetterClient.prototype.grab_chunks =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.chunk_getter.ChunkGetter/grab_chunks',
      request,
      metadata || {},
      methodDescriptor_ChunkGetter_grab_chunks);
};


/**
 * @param {!proto.endpoints.silstn.chunk_getter.ChunkGetter_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.chunk_getter.ChunkGetter_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.chunk_getter.ChunkGetterPromiseClient.prototype.grab_chunks =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.chunk_getter.ChunkGetter/grab_chunks',
      request,
      metadata || {},
      methodDescriptor_ChunkGetter_grab_chunks);
};


module.exports = proto.endpoints.silstn.chunk_getter;

