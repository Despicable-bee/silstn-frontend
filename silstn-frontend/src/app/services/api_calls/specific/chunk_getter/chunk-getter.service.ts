// ANGULAR ---------------------------------------------------------------------

import { 
  Injectable 
} from '@angular/core';

// HELPERS ---------------------------------------------------------------------

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';


// INTERFACES ------------------------------------------------------------------

import {
  ChunkGetter,
  ChunkGetterStream
} from 'src/app/services/api_calls/specific/chunk_getter/chunk_getter.interface';

// ENUMS -----------------------------------------------------------------------

import {
  GrabChunksStatus
} from 'src/app/data_structures/specific/chunk-getter/chunk-getter.ds';

// DATA STRUCTURES -------------------------------------------------------------

import {
  Viewport
} from 'src/app/data_structures/specific/chunk-getter/chunk-getter.ds';

// STUB ------------------------------------------------------------------------

const {
  ChunkGetter_Request,
  ChunkGetter_Response
} = require('src/app/services/api_calls/specific/chunk_getter/chunk_getter_pb')

const {
  ChunkGetterClient
} = require('src/app/services/api_calls/specific/chunk_getter/chunk_getter_grpc_web_pb')

@Injectable({
  providedIn: 'root'
})
export class ChunkGetterService {

  constructor(private loginHelper: LoginHelperService) { }

  // public grab_chunks(_this: ChunkGetter, inputData: Viewport) {
  //   let ChunkService = new ChunkGetterClient(
  //       'https://espv2-chunk-getter-7nbpg3wsnq-ts.a.run.app');

  //   console.log("Do we get here?");

  //   let request = new ChunkGetter_Request();

  //   request.setMaxlat(inputData.maxLat);
  //   request.setMaxlng(inputData.maxLng);
  //   request.setMinlat(inputData.minLat);
  //   request.setMinlng(inputData.minLng);
  //   request.setChunktype(inputData.chunkType);

  //   let metadata = {'authorization': "Bearer " + 
  //       this.loginHelper.get_jwt() };

  //     ChunkService.grab_chunks(request, metadata, 
  //       function(err, response: typeof ChunkGetter_Response) {
  //     if(err != null) {
  //       _this.grab_chunks_error_callback(err);
  //     } else {
  //       const responseArray = response['array'];
  //       switch(responseArray[0]) {
  //         case GrabChunksStatus.SUCCESSFUL:
  //           _this.grab_chunks_success_callback(responseArray[1]);
  //           break;
  //         case GrabChunksStatus.FAILURE:
  //           _this.grab_chunks_unsuccessful_callback(responseArray[2]);
  //           break;
  //         case GrabChunksStatus.ERROR:
  //           _this.grab_chunks_error_callback(responseArray[2]);
  //           break;
  //         default:
  //           console.error("Error getting the chunks");
  //       }
  //     }
  //   });
  // }

  /**
   * Requests a stream of chunks that are within the users viewport.
   * @param _this Reference to the object that implements the given interface
   *    (so this method may invoke the various callbacks)
   * @param inputData The variable containing the users viewport parameters.
   */
  public grab_chunks_stream(_this: ChunkGetterStream, inputData: Viewport) {
    let ChunkService = new ChunkGetterClient(
        'https://espv2-chunk-getter-7nbpg3wsnq-ts.a.run.app');
    
    // Fill out request inputs
      let request = new ChunkGetter_Request();

      request.setMaxlat(inputData.maxLat);
      request.setMaxlng(inputData.maxLng);
      request.setMinlat(inputData.minLat);
      request.setMinlng(inputData.minLng);
      request.setChunktype(inputData.chunkType);

      let metadata = {'authorization': "Bearer " + 
          this.loginHelper.get_jwt() };

      let call = ChunkService.grab_chunks(request, metadata);

      // Handles actual data callbacks.
      call.on('data', (data) => {
        _this.grab_chunks_on_data_callback(data);
      });

      // Handle 'end of stream' callback
      call.on('end', () => {
        _this.grab_chunks_on_end_callback();
      });

      // Handle error callback
      call.on('error', (e) => {
        _this.grab_chunks_on_error_callback(e);
      });

      // Handle process status callback
      call.on('status', (status) => {
        _this.grab_chunks_on_status_callback(status);
      });
  }
}
