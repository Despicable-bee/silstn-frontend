import { TestBed } from '@angular/core/testing';

import { ChunkGetterService } from './chunk-getter.service';

describe('ChunkGetterService', () => {
  let service: ChunkGetterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChunkGetterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
