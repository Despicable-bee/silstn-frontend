import {
    ApiIdMaster
} from 'src/app/services/api_id_master.ds';

import {
    Viewport
} from 'src/app/data_structures/specific/chunk-getter/chunk-getter.ds';

export interface ChunkGetter {
    request_grab_chunks(): void;
    grab_chunks_success_callback(data: string): void;
    grab_chunks_unsuccessful_callback(errMsg: string): void;
    grab_chunks_error_callback(err: any): void;
    grabChunksApiMasterId: ApiIdMaster;
    grabChunksNumRetries: number;
    grabChunksInputData: Viewport;
}

export interface ChunkGetterStream {
    request_grab_chunks_stream(): void;
    grab_chunks_on_data_callback(data: string): void;
    grab_chunks_on_end_callback(): void;
    grab_chunks_on_error_callback(errMsg: string): void;
    grab_chunks_on_status_callback(status): void;
    grabChunksStreamApiMasterId: ApiIdMaster;
    grabChunksStreamNumRetries: number;
    grabChunksStreamInputData: Viewport;
}