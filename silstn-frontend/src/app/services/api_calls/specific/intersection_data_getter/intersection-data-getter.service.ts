// ANGULAR ---------------------------------------------------------------------

import { 
  Injectable 
} from '@angular/core';

// HELPERS ---------------------------------------------------------------------

import {
  LoginHelperService
} from 'src/app/services/helper_methods/shared/login_helper/login-helper.service';

// INTERFACES ------------------------------------------------------------------

import {
  CheckAvailableRanges,
  GetIntersectionData,
  GetForcast
} from 'src/app/services/api_calls/specific/intersection_data_getter/intersection_data_getter.interface';

// ENUMS -----------------------------------------------------------------------

import {
  IntersectionTimeRange,
  CheckAvailableRangesStatus
} from 'src/app/data_structures/specific/intersection_data_getter/intersection_data_getter.ds';

// DATA STRUCTURES -------------------------------------------------------------

import {
  IntersectionDataGetterRequestContainer,
  GetForecastRequestContainer
} from 'src/app/data_structures/specific/intersection_data_getter/intersection_data_getter.ds';

// STUB ------------------------------------------------------------------------

const {
  IntersectionDataGetter_Request,
  IntersectionDataGetter_Response,
  CheckAvailableRange_Request,
  CheckAvailableRange_Response,
  IntersectionForecast_Request,
  IntersectionForecast_Response
} = require('src/app/services/api_calls/specific/intersection_data_getter/intersection_data_getter_pb')

const {
  IntersectionDataGetterClient
} = require('src/app/services/api_calls/specific/intersection_data_getter/intersection_data_getter_grpc_web_pb')

@Injectable({
  providedIn: 'root'
})
export class IntersectionDataGetterService {

  constructor(private loginHelper: LoginHelperService) { }

  // * PUBLIC METHODS ----------------------------------------------------------

  public get_intersection_data_stream(_this: GetIntersectionData, 
      inputData: IntersectionDataGetterRequestContainer) {
    let intersectionService = new IntersectionDataGetterClient(
          "https://espv2-intersection-data-getter-7nbpg3wsnq-ts.a.run.app");
    
    // Fill out the request inputs
    let request = new IntersectionDataGetter_Request();

    request.setIntersectionnum(inputData.tsc);
    request.setTimerangeoption(inputData.timeRange);

    let metadata = {'authorization': "Bearer " + 
          this.loginHelper.get_jwt() };

    let call = intersectionService.get_intersection_data(request, metadata);

    // Handles actual data callbacks
    call.on('data', (data) => {
      _this.get_intersection_data_on_data_callback(data);
    });

    // Handle 'end of stream' callback
    call.on('end', () => {
      _this.get_intersection_data_on_end_callback();
    });

    // Handle error callback
    call.on('error', (e) => {
      _this.get_intersection_data_on_error_callback(e);
    });

    // Handle process status callback
    call.on('status', (status) => {
      _this.get_intersection_data_on_status_callback(status);
    });
  }

  public check_available_ranges(_this: CheckAvailableRanges) {
    let intersectionService = new IntersectionDataGetterClient(
      "https://espv2-intersection-data-getter-7nbpg3wsnq-ts.a.run.app");
    
    // Create a request object (we don't actually have any inputs though :3)
    let request = new CheckAvailableRange_Request();

    let metadata = {'authorization': "Bearer " + 
          this.loginHelper.get_jwt() };

    intersectionService.check_available_ranges(request, metadata,
        function(err, response: typeof CheckAvailableRange_Response) {
      if(err != null) {
        _this.check_available_ranges_error_callback(err);
      } else {
        const responseArray = response['array'];
        console.log(responseArray);
        switch(responseArray[0]) {
          case CheckAvailableRangesStatus.SUCCESSFUL:
            _this.check_available_ranges_success_callback(responseArray[1]);
            break;
          case CheckAvailableRangesStatus.FAILURE:
            _this.check_available_ranges_unsuccessful_callback(responseArray[2]);
            break;
          case CheckAvailableRangesStatus.ERROR:
            _this.check_available_ranges_error_callback(responseArray[2])
            break;
          default:
            console.error("Error getting available ranges");
        }
      }
    });
  }

  public get_forecast(_this: GetForcast, 
      inputData: GetForecastRequestContainer) {
        let intersectionService = new IntersectionDataGetterClient(
          "https://espv2-intersection-data-getter-7nbpg3wsnq-ts.a.run.app");
    
    // Fill out the request inputs
    let request = new IntersectionForecast_Request();

    request.setIntersectionnum(inputData.tsc);

    let metadata = {'authorization': "Bearer " + 
          this.loginHelper.get_jwt() };

    let call = intersectionService.get_forecast(request, metadata);

    // Handles actual data callbacks
    call.on('data', (data) => {
      _this.get_forecast_on_data_callback(data);
    });

    // Handle 'end of stream' callback
    call.on('end', () => {
      _this.get_forecast_on_end_callback();
    });

    // Handle error callback
    call.on('error', (e) => {
      _this.get_forecast_on_error_callback(e);
    });

    // Handle process status callback
    call.on('status', (status) => {
      _this.get_forecast_on_status_callback(status);
    });
  }

  // * PUBLIC VARIABLES --------------------------------------------------------

  // * PRIVATE METHODS ---------------------------------------------------------

  // * PRIVATE VARIABLES -------------------------------------------------------
}
