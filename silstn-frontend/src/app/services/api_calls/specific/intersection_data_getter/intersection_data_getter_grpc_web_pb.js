/**
 * @fileoverview gRPC-Web generated client stub for endpoints.silstn.intersection_data_getter
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.endpoints = {};
proto.endpoints.silstn = {};
proto.endpoints.silstn.intersection_data_getter = require('./intersection_data_getter_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response>}
 */
const methodDescriptor_IntersectionDataGetter_get_intersection_data = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_intersection_data',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request,
  proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response>}
 */
const methodInfo_IntersectionDataGetter_get_intersection_data = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterClient.prototype.get_intersection_data =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_intersection_data',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_get_intersection_data);
};


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetter_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterPromiseClient.prototype.get_intersection_data =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_intersection_data',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_get_intersection_data);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response>}
 */
const methodDescriptor_IntersectionDataGetter_check_available_ranges = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/check_available_ranges',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request,
  proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response>}
 */
const methodInfo_IntersectionDataGetter_check_available_ranges = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterClient.prototype.check_available_ranges =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/check_available_ranges',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_check_available_ranges,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.intersection_data_getter.CheckAvailableRange_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterPromiseClient.prototype.check_available_ranges =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/check_available_ranges',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_check_available_ranges);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response>}
 */
const methodDescriptor_IntersectionDataGetter_get_forecast = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_forecast',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request,
  proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request,
 *   !proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response>}
 */
const methodInfo_IntersectionDataGetter_get_forecast = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response,
  /**
   * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterClient.prototype.get_forecast =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_forecast',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_get_forecast);
};


/**
 * @param {!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Request} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.intersection_data_getter.IntersectionForecast_Response>}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.intersection_data_getter.IntersectionDataGetterPromiseClient.prototype.get_forecast =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/endpoints.silstn.intersection_data_getter.IntersectionDataGetter/get_forecast',
      request,
      metadata || {},
      methodDescriptor_IntersectionDataGetter_get_forecast);
};


module.exports = proto.endpoints.silstn.intersection_data_getter;

