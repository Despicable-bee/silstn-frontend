import { TestBed } from '@angular/core/testing';

import { IntersectionDataGetterService } from './intersection-data-getter.service';

describe('IntersectionDataGetterService', () => {
  let service: IntersectionDataGetterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IntersectionDataGetterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
