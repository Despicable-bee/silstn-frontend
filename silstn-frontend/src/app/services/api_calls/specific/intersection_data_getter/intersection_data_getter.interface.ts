import {
    ApiIdMaster
} from 'src/app/services/api_id_master.ds';

import {
    IntersectionDataGetterRequestContainer,
    GetForecastRequestContainer
} from 'src/app/data_structures/specific/intersection_data_getter/intersection_data_getter.ds';

export interface GetIntersectionData {
    request_get_intersection_data_stream(): void;
    get_intersection_data_on_data_callback(data: string): void;
    get_intersection_data_on_end_callback(): void;
    get_intersection_data_on_error_callback(errMsg: string): void;
    get_intersection_data_on_status_callback(status): void;
    getIntersectionDataStreamApiMasterId: ApiIdMaster;
    getIntersectionDataStreamNumRetries: number;
    getIntersectionInputData: IntersectionDataGetterRequestContainer;
}

export interface CheckAvailableRanges {
    request_check_available_ranges(): void;
    check_available_ranges_success_callback(data: number): void;
    check_available_ranges_unsuccessful_callback(errMsg: string): void;
    check_available_ranges_error_callback(err: any): void;
    checkAvailableRangesApiMasterId: ApiIdMaster;
    checkAvailableRangesNumRetries: number;
}

export interface GetForcast {
    request_get_forecast(): void;
    get_forecast_on_data_callback(data: string): void;
    get_forecast_on_end_callback(): void;
    get_forecast_on_error_callback(errMsg: string): void;
    get_forecast_on_status_callback(status): void;
    getForecastApiMasterId: ApiIdMaster;
    getForecastNumNumRetries: number;
    getForecastInputData: GetForecastRequestContainer;
}