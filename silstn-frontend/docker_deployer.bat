@ECHO OFF
:: This batch file generates the docker container
TITLE SILSTN docker deployer
ECHO ----------------------------------------------------------------------
ECHO SILSTN's Google Cloud Run deployer
ECHO Deploying container...
CMD /c docker push gcr.io/silstn-development/frontend:latest

gcloud run deploy frontend --image^
    gcr.io/silstn-development/frontend:latest
    --platform managed^
    --region australia-southeast1
ECHO Done
ECHO ----------------------------------------------------------------------
PAUSE