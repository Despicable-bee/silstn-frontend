@ECHO OFF
:: This batch file generates the docker container
:: Despicable_bee
TITLE Starfighter docker builder
ECHO ----------------------------------------------------------------------
ECHO SILSTN Frontend builder
ECHO Building container...
CMD /c docker build -f dockerfile ^
    -t gcr.io/silstn-development/frontend:latest .
ECHO Done
ECHO ----------------------------------------------------------------------
PAUSE